# MOBA Server #

This is a little MOBA made completely in Java. Here you can find the [client](https://bitbucket.org/Pole458/moba-client/overview) and the [Map Creator](https://bitbucket.org/Pole458/moba-map-creator).

The server can host up to 16 players, managing players interactions during lobbies, champion select and games.

## The Game ##

This game can be played up to 6 people in a 3v3 mode. In order to win, each team has to carry a "ball" in the other's team base. The first team that scores two points wins the game.

You can also find a video trailer here:

[![IMAGE ALT TEXT HERE](http://img.youtube.com/vi/KT5oj_REpf0/0.jpg)](http://www.youtube.com/watch?v=KT5oj_REpf0)

## Credits ##

Made by Pole.

Sprites ripped from the game "Lock's Quest", thanks to the guys on https://www.spriters-resource.com/
