package game;

import java.awt.Point;
import java.awt.Rectangle;
import projectiles.Projectile;
import Champions.Champion;

public class Unit {
	
	protected Game game;
	protected String name;
	protected char team;
	protected Unit target;
	protected Unit damager;
	
	//movement
	protected double hSpeed;
	protected double vSpeed;
	protected short direction;
	protected double x;
	protected double y;
	
	//drawings
	protected short sprite;		// <- ???
	protected short spriteIndex;// <- ???
	
	protected double frameIndex;

	protected short upDistance;
	protected short downDistance;
	
	//collision
	protected short width;
	
	//stats
	protected short HPMax;
	protected short armor;
	protected short damage;
	protected short attTimer;
	protected short lifesteal;
	protected double speed;
	protected short range;
	
	protected double HP;
	protected long attLastCast;
	
	//stat bonuses
	protected short HPMaxBonus;
	protected short armorBonus;
	protected short damageBonus;
	protected short attSpdBonus;
	protected short lifestealBonus;
	protected short speedBonus;
	protected short rangeBonus;
	
	protected short newHPMaxBonus;
	protected short newArmorBonus;
	protected short newDamageBonus;
	protected short newAttSpdBonus;
	protected short newLifestealBonus;
	protected short newSpeedBonus;
	protected short newRangeBonus;
	
	protected String status;
	protected short statusTimer;
	protected long statusLastCast;
	
	protected short damageReceived;
	protected long timeDamageReceived;
	
	//CONSTRUCTORS********************************************************************************************************************************
	public Unit( Game game ) {
		
		this.game = game;
		timeDamageReceived=System.currentTimeMillis();
	}
	
	/*public Unit(Unit u)	{
		this.game = u.game;
		this.name=u.name;
		this.hSpeed=u.hSpeed;
		this.vSpeed=u.vSpeed;
		this.direction=u.direction;
		this.x=u.x;
		this.y=u.y;
		this.sprite=u.sprite;
		this.frameIndex=u.frameIndex;
		this.spriteIndex=u.spriteIndex;
		this.upDistance=u.upDistance;
		this.width=u.width;
		this.team=u.team;
		this.target=u.target;
		this.damager=u.damager;
		this.HPMax=u.HPMax;
		this.armor=u.armor;
		this.damage=u.damage;
		this.attTimer=u.attTimer;
		this.lifesteal=u.lifesteal;
		this.speed=u.speed;
		this.range=u.range;
		this.HP=u.HP;
		this.attLastCast=u.attLastCast;
		this.HPMaxBonus=u.HPMaxBonus;
		this.armorBonus=u.armorBonus;
		this.damageBonus=u.damageBonus;
		this.attSpdBonus=u.attSpdBonus;
		this.lifestealBonus=u.lifestealBonus;
		this.speedBonus=u.speedBonus;
		this.rangeBonus=u.rangeBonus;
		this.status=u.status;
		this.statusTimer=u.statusTimer;
		this.statusLastCast=u.statusLastCast;
		this.damageReceived=u.damageReceived;
		this.timeDamageReceived=u.damageReceived;
	}*/
	//CONSTRUCTORS********************************************************************************************************************************
	//STEP********************************************************************************************************************************
	public void update() {
		
		if(HP>getHPMax()) HP=getHPMax();
		if(HP<0) HP=0;
		
		if(250+timeDamageReceived<System.currentTimeMillis()) {
			damageReceived=0;
		}
	}
	//STEP******************************************************************************************************************************
	//UNIT UTILITIES******************************************************************************************************************************
	public int getAttCD() {
		return (int)((System.currentTimeMillis()-attLastCast)/attTimer*100);
	}
	
	public void setHP(double HP) {
		if(this.HP>HP) {
			damageReceived+=(short)(this.HP-HP);
			timeDamageReceived=System.currentTimeMillis();
		}
		this.HP=HP;
	}
	
	public short getDamageReceived() {
		return (short)(damageReceived*(100-(System.currentTimeMillis()-timeDamageReceived)/2.5)/100);
	}
	
	public void setDamager(Unit u) {
		this.damager=u;
	}
	
	public void setDirection(short direction) {
		this.direction=direction;
	}
	
	public void setStatus(String status,short statusTimer) {
		this.status=status;
		this.statusTimer=statusTimer;
		statusLastCast=System.currentTimeMillis();
	}
	
	public String getStatus() {
		return status;
	}
	
	public static boolean isVisible(Unit u) {
		return u.getVisible();
	}
	
	public void onHit(Unit target) {}
	
	public void spellHit(Unit target) {}
	
	public void applyLifeSteal(double damageDealt) {}
	
	public boolean isDeath() {
		if(HP<=0) return true;
		else return false;
	}
	
	public boolean isAlly(Unit u) {
		if(team==u.team && u.isAlive()) return true;
		else return false;
	}
	
	public boolean isAlive() {
		if(getStatus().equals("death")) return false;
		else return true;
	}
	
	public boolean isTargettable(Unit u) {
		if(!isAlly(u) && u.isAlive() && isVisible(u)) return true;
		else return false;
	}
	
	public boolean isDamageable(Unit u) {
		if(!isAlly(u) && u.isAlive()) return true;
		else return false;
	}
	
	public double distanceToUnit(Unit u)  {
		return Point.distance(getX(),getY(), u.getX(), u.getY());
	}
	
	public double distanceToProjectile(Projectile p)  {
		return Point.distance(getX(),getY(), p.getX(), p.getY());
	}
	
	public boolean isAChampion() {
		return this.getClass().getSuperclass().equals(Champion.class);
	}
	
	public Unit nearestUnit() {
		int nearest=0;
		while(nearest<game.units.length && (game.units[nearest].equals(this) || !game.units[nearest].isAlive()) )
			nearest++;
		
		int next=nearest+1;
		while(next<game.units.length && (game.units[next].equals(this) || !game.units[next].isAlive()) )
			next++;
		
		while(next<game.units.length) {
			if(Point.distance(x,y,game.units[next].getX(),game.units[next].getY())<=Point.distance(x,y,game.units[nearest].getX(),game.units[nearest].getY()))
				nearest=next;
				next++;
			while(next<game.units.length && (game.units[next].equals(this) || !game.units[next].isAlive()) )
				next++;
		}
	 
		if(nearest<game.units.length) return game.units[nearest];
		else return null;
	}
	
	public Unit nearestEnemyUnit() {
		int nearest=0;
		while(nearest<game.units.length && !isTargettable(game.units[nearest]))
			nearest++;
		
		int next=nearest+1;
		while(next<game.units.length && !isTargettable(game.units[next]) )
			next++;
		
		while(next<game.units.length) {
			if(Point.distance(x,y,game.units[next].getX(),game.units[next].getY())<=Point.distance(x,y,game.units[nearest].getX(),game.units[nearest].getY()))
				nearest=next;
				next++;
				while(next<game.units.length && !isTargettable(game.units[next]) )
					next++;
		}
	 
		if(nearest<game.units.length) return game.units[nearest];
		else return null;
	}
	
	public Unit nearestAllyUnit() {
		int nearest=0;
		while(nearest<game.units.length && !isAlly(game.units[nearest]))
			nearest++;
		
		int next=nearest+1;
		while(next<game.units.length && !isAlly(game.units[next]) )
			next++;
		
		while(next<game.units.length) {
			if(Point.distance(x,y,game.units[next].getX(),game.units[next].getY())<=Point.distance(x,y,game.units[nearest].getX(),game.units[nearest].getY()))
				nearest=next;
				next++;
				while(next<game.units.length && !isAlly(game.units[next]) )
					next++;
		}
	 
		if(nearest<game.units.length) return game.units[nearest];
		else return null;
	}
	
	public Unit nearestEnemyUnitExcept(Unit unit) {
		int nearest=0;
		while(nearest<game.units.length && (!isTargettable(game.units[nearest]) || game.units[nearest].equals(unit)) )
			nearest++;
		
		int next=nearest+1;
		while(next<game.units.length && (!isTargettable(game.units[next]) || game.units[next].equals(unit)) )
			next++;
		
		while(next<game.units.length) {
			if(Point.distance(x,y,game.units[next].getX(),game.units[next].getY())<=Point.distance(x,y,game.units[nearest].getX(),game.units[nearest].getY()))
				nearest=next;
				next++;
				while(next<game.units.length && (!isTargettable(game.units[next]) || game.units[next].equals(unit)) )
					next++;
		}
	 
		if(nearest<game.units.length) return game.units[nearest];
		else return null;
	}
	
	public Unit nearestAllyUnitExcept(Unit unit) {
		int nearest=0;
		while(nearest<game.units.length && (!isAlly(game.units[nearest]) || game.units[nearest].equals(unit)) )
			nearest++;
		
		int next=nearest+1;
		while(next<game.units.length && (!isAlly(game.units[next]) || game.units[next].equals(unit)) )
			next++;
		
		while(next<game.units.length) {
			if(Point.distance(x,y,game.units[next].getX(),game.units[next].getY())<=Point.distance(x,y,game.units[nearest].getX(),game.units[nearest].getY()))
				nearest=next;
				next++;
				while(next<game.units.length && (!isAlly(game.units[next]) || game.units[next].equals(unit)) )
					next++;
		}
	 
		if(nearest<game.units.length) return game.units[nearest];
		else return null;
	}
	
	public Unit nearestEnemyUnitExcept(Unit[] units) {
		int nearest=0;
		while(nearest<game.units.length && (!isTargettable(game.units[nearest]) || game.units[nearest].isInThisList(units)) )
			nearest++;
		
		int next=nearest+1;
		while(next<game.units.length && (!isTargettable(game.units[next]) || game.units[next].isInThisList(units)) )
			next++;
		
		while(next<game.units.length) {
			if(Point.distance(x,y,game.units[next].getX(),game.units[next].getY())<=Point.distance(x,y,game.units[nearest].getX(),game.units[nearest].getY()))
				nearest=next;
				next++;
				while(next<game.units.length && (!isTargettable(game.units[next]) || game.units[next].isInThisList(units)) )
					next++;
		}
	 
		if(nearest<game.units.length) return game.units[nearest];
		else return null;
	}
	
	public boolean isInThisList(Unit[] units) {
		if(units==null) return false;
		for(short i=0; i<units.length; i++)
			if(equals(units[i])) return true;
		return false;
	}
	
	public Unit[] getEnemyAoE(double x, double y, short radius) {
		Unit[] units = new Unit[0];
		Unit[] u;
		for(short i=0; i<game.units.length;i++)
			if(isDamageable(game.units[i]) && !isAlly(game.units[i]) && Point.distance(x,y,game.units[i].getX(),game.units[i].getY())<radius) {
				u = units;
				units = new Unit[units.length+1];
				for(short j=0; j<units.length-1;j++)
					units[j] = u[j];
				units[units.length-1]=game.units[i];
			}
		return units;
	}
	
	public Unit[] getAllyAoE(double x, double y, short radius) {
		Unit[] units = new Unit[0];
		Unit[] u;
		for(short i=0; i<game.units.length;i++)
			if(isAlly(game.units[i]) && !game.units[i].equals(this) && Point.distance(x,y,game.units[i].getX(),game.units[i].getY())<radius) {
				u = units;
				units = new Unit[units.length+1];
				for(short j=0; j<units.length-1;j++)
					units[j] = u[j];
				units[units.length-1]=game.units[i];
			}
		return units;
	}
	
	public short getPointDirection(double x, double y) {
		short degrees = (short)(Math.atan((y-this.y)/(this.x-x))*180/Math.PI);
		
		if(x<this.x) degrees+=180;
		if(x>=this.x && y>this.y) degrees+=360;
		
		return degrees;
	}
	
	public short getPointDirection45(double x, double y) {
		short degrees = (short)(Math.atan((y-this.y)/(this.x-x))*180/Math.PI);
		
		if(x<this.x) degrees+=180;
		if(x>=this.x && y>this.y) degrees+=360;
		
		return getDirection45(degrees);
	}
	
	public short getDirection45(short degrees) {
		if(degrees>337.5 || degrees<22.5) degrees=0;
		if(degrees>22.5 && degrees<67.5) degrees=45;
		if(degrees>67.5 && degrees<112.5) degrees=90;
		if(degrees>112.5 && degrees<157.5) degrees=135;
		if(degrees>157.5 && degrees<202.5) degrees=180;
		if(degrees>202.5 && degrees<247.5) degrees=225;
		if(degrees>247.5 && degrees<292.5) degrees=270;
		if(degrees>292.5 && degrees<337.5) degrees=315;
		
		return degrees;
	}
	//UNIT UTILITIES*****************************************************************************************************************
	//DAMAGING****************************************************************************************************************************
	public double damaging(Unit target, double damage, boolean onHitProc, boolean spellHitProc) {
		if(target.isAlive() && isDamageable(target)) {
			double damageDealt = (int)(damage*(100-target.getArmor())/100);
			target.setHP(target.getHP()-damageDealt);
			target.setDamager(this);
			if(onHitProc) {
				onHit(target);
				applyLifeSteal(damageDealt);
			}
			if(spellHitProc) spellHit(target);
			return damageDealt;
		} else return 0;
	}
	
	public double damagingAoE(double x, double y, int radius, double damage, boolean onHitProc, boolean spellHitProc) {
		double damageDealt=0;
		for(int i=0; i<game.units.length;i++)
			if(isDamageable(game.units[i]) && Point.distance(x,y,game.units[i].getX(),game.units[i].getY())<radius) {
				damageDealt+=damaging(game.units[i],damage,onHitProc,spellHitProc);
			}
		return damageDealt;
	}
	//DAMAGING****************************************************************************************************************************
	//GET STATS************************************************************************************************************************
	public String getName() {
		return name;
	}
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	public short getSprite() {
		return sprite;
	}
	public short getFrame() {
		return (short)(spriteIndex+(short)(frameIndex)/10);
	}
	
	public short getWidth() {
		return width;
	}
	public char getTeam() {
		return team;
	}
	public boolean getVisible() {
		return true;
	}
	public String getBonus() {
		return "noone";
	}
	public Rectangle getSize() {
	    return new Rectangle((int)x-width,(int)y-width,width*2,width*2);
	}
	public void resetPosition() {
	}
	
	public short getDownDistance() {
		return downDistance;
	}
	
	public short getUpDistance() {
		return upDistance;
	}
	
	public short getHPMax() {
		return (short)(HPMax*HPMaxBonus/100);
	}
	public short getArmor() {
		return (short)(armor*armorBonus/100);
	}
	public short getDamage() {
		return (short)(damage*damageBonus/100);
	}
	public short getAttTimer() {
		return (short)(attTimer/attSpdBonus*100);
	}
	public short getLifeSteal() {
		return (short)(lifesteal*lifestealBonus/100);
	}
	public double getSpeed() {
		return speed*speedBonus/100;
	}
	public short getRange() {
		return (short)(range*rangeBonus/100);
	}
	public double getHP() {
		return HP;
	}
	//GET STATS************************************************************************************************************************
	//BONUS STATS************************************************************************************************************************
	public void resetBonus() {
    	HPMaxBonus=100;
    	armorBonus=100;
    	damageBonus=100;
    	attSpdBonus=100;
    	lifestealBonus=100;
    	speedBonus=100;
    	rangeBonus=100;
    }
	public void setHPMaxBonus(int bonus) {
    	HPMaxBonus+=bonus;
    }
    public void setArmorBonus(int bonus) {
    	armorBonus+=bonus;
    }
    public void setDamageBonus(int bonus) {
    	damageBonus+=bonus;
    }
    public void setAttSpdBonus(int bonus) {
    	attSpdBonus+=bonus;
    }
    public void setLifeStealBonus(int bonus) {
    	lifestealBonus+=bonus;
    }
    public void setSpeedBonus(int bonus) {
    	speedBonus+=bonus;
    }
    public void setRangeBonus(int bonus) {
    	rangeBonus+=bonus;
    }
    
    public void newHPMaxBonus(int bonus) {
    	newHPMaxBonus+=bonus;
    }
    public void newArmorBonus(int bonus) {
    	newArmorBonus+=bonus;
    }
    public void newDamageBonus(int bonus) {
    	newDamageBonus+=bonus;
    }
    public void newAttSpdBonus(int bonus) {
    	newAttSpdBonus+=bonus;
    }
    public void newLifeStealBonus(int bonus) {
    	newLifestealBonus+=bonus;
    }
    public void newSpeedBonus(int bonus) {
    	newSpeedBonus+=bonus;
    }
    public void newRangeBonus(int bonus) {
    	newRangeBonus+=bonus;
    }
    
    public void applyBonus() {
    	HPMaxBonus+=newHPMaxBonus;
    	newHPMaxBonus=0;
    	armorBonus+=newArmorBonus;
    	newArmorBonus=0;
    	damageBonus+=newDamageBonus;
    	newDamageBonus=0;
    	attSpdBonus+=newAttSpdBonus;
    	newAttSpdBonus=0;
    	lifestealBonus+=newLifestealBonus;
    	newLifestealBonus=0;
    	speedBonus+=newSpeedBonus;
    	newSpeedBonus=0;
    	rangeBonus+=newRangeBonus;
    	newRangeBonus=0;
    }
    //BONUS STATS***********************************************************************************************************************
}