package game;

public class UpdateUnit {
	
	private Unit unit;
	
	private boolean created;
	
	public UpdateUnit(Unit u) {
		created = true;
		unit = u;
	}

	public boolean getCreated() {
		return created;
	}
	
	public void setCreated(boolean c) {
		created = c;
	}
	
	public Unit getUnit() {
		return unit;
	}
	
	/*public short getDeltaX() {
		return (short)(unit.getX() - x);
	}
	
	public short getDeltaY() {
		return (short)(unit.getY() - y);
	}
	
	public short getDeltaFrame() {
		return (short)(unit.getFrame() - frame);
	}
	
	public boolean getVisible() {
		return unit.getVisible();
	}
	
	public short getDeltaHPMax() {
		return (short)(unit.getHPMax() - HPMax);
	}
	
	public short getDeltaHP() {
		return (short)(unit.getHP() - HP);
	}*/
}
