package game;

import java.awt.Rectangle;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import projectiles.Bomb;
import projectiles.Projectile;
import server.ChampionSelect;
import server.Server;
import server.User;
import Champions.*;

public class Game implements Runnable {
	
	public Thread gameThread;
	private final int UPDATERATE = 20;
	
	public Map map;
	
	private User[] players;
	private boolean[] playersConnected;
	
	public Unit[] units;
	private Champion[] champions;
	
	private UpdateUnit[] updateUnits;
	
	public Projectile[] projs;
	private Bomb bomb;
	
	private UpdateProjectile[] updateProjs;
	
	private short blueScore;
	private short redScore;
	private boolean scored;
	private short scoredPause;
	private long scoredMoment;
	
	//STARTING THE GAME***************************************************************************************************************************
	public Game(ChampionSelect c) {
		
		Server.DebugLog(" - Creating Game -\n");
		
		blueScore=0;
		redScore=0;
		scored=false;
		scoredPause=3000;
		scoredMoment=System.currentTimeMillis()-scoredPause-1;
		
        map = new Map( this );
        
		players = c.getUsers();
		
		bomb = new Bomb(21*24,22*24-12,null);
		
		projs = new Projectile[1];
		projs[0] = bomb;
		
		units = new Unit[players.length];
		champions = new Champion[players.length];
		playersConnected = new boolean[players.length];
		
		for(short i=0; i<units.length;i++) {
			
			c.getUser(i).setStatus("Game");
				
			switch(c.getChampion(i)) {
			case 0:
				units[i] = new Nightdart(this, c.getUser(i).getUserName(),c.getUserTeam(i));
				break;
			case 1:
				units[i] = new Purprowl(this, c.getUser(i).getUserName(),c.getUserTeam(i));
				break;
			case 2:
				units[i] = new Bullbot(this, c.getUser(i).getUserName(),c.getUserTeam(i));
				break;
			case 3:
				units[i] = new Hairwitch(this, c.getUser(i).getUserName(),c.getUserTeam(i));
				break;
			case 4:
				units[i] = new LordAgony(this, c.getUser(i).getUserName(),c.getUserTeam(i));
				break;
			case 5:
				units[i] = new Bluerred(this, c.getUser(i).getUserName(),c.getUserTeam(i));
				break;
			case 6:
				units[i] = new Steamstoove(this, c.getUser(i).getUserName(),c.getUserTeam(i));
				break;
			case 7:
				units[i] = new Swordensheeld(this, c.getUser(i).getUserName(),c.getUserTeam(i));
				break;
			case 8:
				units[i] = new Onslaught(this, c.getUser(i).getUserName(),c.getUserTeam(i));
				break;
			case 9:
				units[i] = new Spike(this, c.getUser(i).getUserName(),c.getUserTeam(i));
				break;
			case 10:
				units[i] = new Mastermind(this, c.getUser(i).getUserName(),c.getUserTeam(i));
				break;
			case 11:
				units[i] = new Bullseye(this, c.getUser(i).getUserName(),c.getUserTeam(i));
				break;
			}
			
			champions[i] = (Champion)units[i];
			playersConnected[i] = true;
		}
		
        gameThread = new Thread(this,"Game's Thread");
        gameThread.start();
	}
	
	private void sendCreatedData() {
		
		createUpdatedUnits();
		createUpdatedProjs();
		
		for(short i=0; i<players.length; i++) {
				
			try {
					
				if( playersConnected[i] ) {
					
					//creates a packet so send to each player
					ByteArrayOutputStream packet = new ByteArrayOutputStream();
					DataOutputStream stream = new DataOutputStream(packet);
					
					//Tells the client he's sending this package of data
					Server.msgToClient(stream, "CreatedGameData");
					
					stream.write(i);	//tells the user his number
					
					stream.write(map.gates.length);
					
					for(short j=0; j<map.gates.length; j++) {
						stream.write((int)map.gates[j].getTilePoint().getX());
						stream.write((int)map.gates[j].getTilePoint().getY());
						stream.write(map.gates[j].getTile());
					}
					
					sendScore(stream);
					
					sendGates(stream);
					
					sendStats(stream, i);
					
					sendCD(stream, i);
				
					sendUnitsOnCreation(stream, i);
					
					sendProjectilesOnCreation(stream);
					
					players[i].sendToClient( packet );		//actually sends the packet
					
				}
			} catch(IOException e) {
				//Server.DebugLog("Error! Could not send gameUpdated data to: " + players[i].getUserName() +'\n') ;
			}
		}
		
		Server.DebugLog("Created Game data sent\n");
	}

	private void sendUnitsOnCreation(DataOutputStream stream, short u) throws IOException {
		
		stream.write(updateUnits.length);
		
		for(short j=0; j<updateUnits.length; j++)
				sendCreatedUnit(stream, u, j);
	}

	private void sendProjectilesOnCreation(DataOutputStream stream) throws IOException {
		
		stream.write(updateProjs.length);
		
		for(short j=0; j<updateProjs.length; j++)	
				sendCreatedProjectile(stream, j);
	}

	private void createUpdatedUnits() {
		
		UpdateUnit[] u = new UpdateUnit[units.length];
		
		if(updateUnits == null ) {
			
			for(short i=0; i<u.length; i++)
				u[i] = new UpdateUnit(units[i]);
				
		} else {
			
			short i=0, j=0;
			while( i<updateUnits.length && j<units.length ) {
				
				if( updateUnits[i].getUnit() == units[j] ) {
					u[j] = updateUnits[i];
					u[j].setCreated(false);
					i++;
					j++;
				}
				else {	//the element is missing: that means it was destroyed
					i++;
				}
			
			}
			
			for(; j<units.length; j++) {	//the remaining elements are all new
				u[j] = new UpdateUnit(units[j]);
			}
			
		}
		
		updateUnits = u;
		
	}
	
	private void createUpdatedProjs() {
		
		UpdateProjectile[] p = new UpdateProjectile[projs.length];
		
		if(updateProjs == null ) {
			
			for(short i=0; i<p.length; i++)
				p[i] = new UpdateProjectile(projs[i]);
			
		} else {
			
			short i=0, j=0;
			while( i<updateProjs.length && j<projs.length ) {
				
				if( updateProjs[i].getProjectile() == projs[j] ) {
					p[j] = updateProjs[i];
					p[j].setCreated(false);
					i++;
					j++;
				}
				else {	//the element is missing: that means it was destroyed
					i++;
				}
			
			}
			
			for(; j<projs.length; j++) {	//the remaining elements are all new
				p[j] = new UpdateProjectile(projs[j]);
			}
			
		}
		
		updateProjs = p;
		
	}
	
	
	//STARTING THE GAME***************************************************************************************************************************
	//GAME MAIN THREAD***************************************************************************************************************************
	@Override
	public void run() {
		
		Server.DebugLog("Thread started\n");
		
		//Waits for 5 seconds before beginning
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			Server.DebugLog(e.getMessage());
		}
	
        sendCreatedData();
        
        long beforeTime, timeDiff, sleep;
        beforeTime = System.currentTimeMillis();
        
        Server.DebugLog("Starting update loop\n");
        
        while( ( blueScore<2 && redScore<2 ) || System.currentTimeMillis() < scoredMoment+scoredPause ) {
        	
        	update();
        	
        	sendUpdateData();
        	
            timeDiff = System.currentTimeMillis() - beforeTime;
            sleep = UPDATERATE - timeDiff;
            
            if (sleep < 0) {
                sleep = 2;
            }
            
            try {
            	Thread.sleep(sleep);
            } catch (InterruptedException e) {
                System.out.println("Interrupted: " + e.getMessage());
            }
            
            beforeTime = System.currentTimeMillis();
    	}
        
        
        try {
        	Thread.sleep(3000);
        } catch (InterruptedException e) {
            System.out.println("Interrupted: " + e.getMessage());
        }
        
        Server.createLobby(this);
        Server.destroyGame(this);
    }
	
	private void update() {
		
		if( blueScore<2 && redScore<2 && System.currentTimeMillis()>scoredMoment+scoredPause) {
    		
    		if(scored) {
    			resetPosition();
    			scored=false;
    		}
    		
    		checkEvents();
    		
    		for(short i=0; i<units.length;i++) {
    			units[i].resetBonus();
    			units[i].applyBonus();
    			units[i].update();
    			if(!units[i].getClass().equals(Champion.class))
    				if(units[i].isDeath()) removeUnit(i);
    		}
    		
    		for(short i=0; i<projs.length;i++)
    			if(!projs[i].update()) removeProjectile(i);
    	}
	}
	
	//GAME MAIN THREAD***************************************************************************************************************************
	//GAME UTILITIES***************************************************************************************************************************
	private void checkEvents() {
		
		//check if one of the teams has scored
		if(new Rectangle(12*24,14*24,24,24).contains(bomb.getX(),bomb.getCollisionY())) {
			redScore++;
			scoredMoment = System.currentTimeMillis();
			bomb = new Bomb( 34*24 + 12, 9*24, null);
			projs[0] = bomb;
			scored=true;            			
		}
		
		if(new Rectangle(29*24,31*24,24,24).contains(bomb.getX(),bomb.getCollisionY())) {
			blueScore++;
			scoredMoment=System.currentTimeMillis();
			bomb = new Bomb( 7*24 + 12, 36*24, null);
			projs[0] = bomb;
			scored=true;
		}
		
		for(short i=0; i<map.gates.length; i++) {
				map.gates[i].checkGate();
		}
		
		for(short i=0; i<champions.length;i++)
			bomb.checkBomb(champions[i]);
	}
	
	private void resetPosition() {
		for(short i=0; i<units.length;i++) {
			units[i].resetPosition();
			units[i].setHP(units[i].getHPMax());
		}
	}
	
	public void addUnit(Unit u) {
		Unit[] temp = units;
		
		units = new Unit[units.length+1];
		
		for(int i=0; i<temp.length; i++) {
			units[i] = temp[i];
		}
		units[units.length-1] = u;
    }
    
    public void removeUnit(short i) {
    	Unit[] temp = units;
    	
    	units = new Unit[units.length-1];
    	
    	for(int j=0; j<units.length;j++) {
			if(j<i)
				units[j] = temp[j];
			else
				units[j] = temp[j+1];
    	}
    }
    
    public void addProjectile(Projectile p) {
    	
    	Projectile[] pro = projs;
		
		projs = new Projectile[projs.length+1];
		
		for(int i=0; i<pro.length; i++) {
			projs[i] = pro[i];
		}
		projs[projs.length-1] = p;
    	
    }
    
    public void removeProjectile(short i) {
    	Projectile[] pro = projs;

    	projs = new Projectile[projs.length-1];
    	
    	for(int j=0; j<projs.length;j++) {
			if(j<i)
				projs[j] = pro[j];
			else
				projs[j] = pro[j+1];
    	}
    }
	//GAME UTILITES**************************************************************************************************************************
	
	public short getPlayers() {
		return (short)players.length;
	}
	
	public User getPlayer(short i) {
		return players[i];
	}
	
	public boolean getPlayerTeam(short i) {
		return champions[i].getTeam() == 'B';
	}
	
	public boolean isInGame(User u) {
		
		for(short i=0; i<players.length; i++)
			if(players[i] == u) return true;
		
		return false;
	}
	
	public boolean isInGame(String s) {
		
		for(short i=0; i<players.length; i++)
			if(players[i].getUserName().equals(s)) return true;
		
		return false;
	}
	
	public void readKeyData(User u) throws IOException {
		
		for(short i=0; i<players.length; i++)
			if(players[i] == u) {
				champions[i].setUpKey(u.inDataStream.readBoolean());
				champions[i].setDownKey(u.inDataStream.readBoolean());
				champions[i].setLeftKey(u.inDataStream.readBoolean());
				champions[i].setRightKey(u.inDataStream.readBoolean());
				champions[i].setJKey(u.inDataStream.readBoolean());
				champions[i].setKKey(u.inDataStream.readBoolean());
				champions[i].setLKey(u.inDataStream.readBoolean());
			}
	}
	
	//OUTPUT TO CLIENT***************************************************************************************************************************
	public void sendUpdateData() {
		
		createUpdatedUnits();
		createUpdatedProjs();
	
		for(short i=0; i<players.length; i++) {		//Send to each player in the game the updated Game data
			
			try {
				
				if( playersConnected[i] ) {

					//creates a packet with the data to send to each player
					ByteArrayOutputStream packet = new ByteArrayOutputStream();
					DataOutputStream stream = new DataOutputStream(packet);
					
					//Tells the client he's sending this package of data
					Server.msgToClient(stream, "GameData");
					
					sendScore(stream);
					
					sendGates(stream);
					
					sendStats(stream, i);
					
					sendCD(stream, i);
				
					sendUnits(stream, i);
					
					sendProjectiles(stream);
					
					players[i].sendToClient( packet );
				
				}
			} catch(IOException e) {
				Server.DebugLog("Error! Could not send gameCreated data to: " + players[i].getUserName() +'\n') ;
			}
		}
	}

	
	private void sendScore(DataOutputStream stream) throws IOException {
		stream.write(blueScore);
		stream.write(redScore);
	}
	
	private void sendStats(DataOutputStream stream, short i) throws IOException {
		
		stream.write(champions[i].getArmor());
		stream.write(champions[i].getDamage());
		stream.writeShort(champions[i].getAttTimer());
		stream.write(champions[i].getLifeSteal());
		stream.writeShort((int)(champions[i].getSpeed()*100));
	}
	
	private void sendUnits(DataOutputStream stream, short u) throws IOException {
		
		stream.write(updateUnits.length);
		
		for(short j=0; j<updateUnits.length; j++) {
			
			if( updateUnits[j].getCreated() )	//if this unit was just created
				sendCreatedUnit(stream, u, j);
			else								//if not, send only the updated informations
				sendUpdatedUnit(stream, u, j);
		}
	}
	
	private void sendCreatedUnit(DataOutputStream stream, short u, short j) throws IOException {
	
		stream.writeBoolean(true);
		
		Server.writeString(stream, updateUnits[j].getUnit().getName() );
		stream.writeChar( updateUnits[j].getUnit().getTeam() );
		
		stream.writeShort( (short) updateUnits[j].getUnit().getX() );
		stream.writeShort( (short) updateUnits[j].getUnit().getY() );
		
		stream.write( updateUnits[j].getUnit().getSprite() );
		stream.write( (int) updateUnits[j].getUnit().getFrame() );
		
		stream.write( updateUnits[j].getUnit().getUpDistance() );
		stream.write( updateUnits[j].getUnit().getDownDistance() );
				
		stream.writeShort( (short) updateUnits[j].getUnit().getHPMax() );
		stream.writeShort( (short) updateUnits[j].getUnit().getHP() );
		
		stream.writeBoolean( updateUnits[j].getUnit().getVisible() && updateUnits[j].getUnit().getTeam() != champions[u].getTeam() );
	}
	
	private void sendUpdatedUnit(DataOutputStream stream, short u, short j) throws IOException {
		
		stream.writeBoolean(false);
		
		stream.writeShort( (short)updateUnits[j].getUnit().getX() );
		stream.writeShort( (short)updateUnits[j].getUnit().getY() );
		
		stream.write( updateUnits[j].getUnit().getFrame() );
		
		stream.write( updateUnits[j].getUnit().getUpDistance() );
		
		stream.writeShort( updateUnits[j].getUnit().getHPMax() );
		stream.writeShort( (short) updateUnits[j].getUnit().getHP() );
		
		stream.writeBoolean( updateUnits[j].getUnit().getVisible() && updateUnits[j].getUnit().getTeam() != champions[u].getTeam() );
	
	}
	
	private void sendProjectiles(DataOutputStream stream) throws IOException {
		
		stream.write(updateProjs.length);
		
		for(short j=0; j<updateProjs.length; j++) {
			
			if( updateProjs[j].getCreated() )	//if this proj was just created
				sendCreatedProjectile(stream, j);
			else 								// if not, send only the updated informations
				sendUpdatedProjectile(stream,j);
		}
	}
	
	private void sendCreatedProjectile(DataOutputStream stream, short j) throws IOException {
		
		stream.writeBoolean(true);
		
		stream.writeChar(updateProjs[j].getProjectile().getTeam());
		
		stream.writeShort( (short) updateProjs[j].getProjectile().getX());
		stream.writeShort( (short) updateProjs[j].getProjectile().getY());
		
		stream.write(updateProjs[j].getProjectile().getSprite());
		stream.write( (int) updateProjs[j].getProjectile().getFrame());
		
		stream.writeShort(updateProjs[j].getProjectile().getRange());
	}
	
	private void sendUpdatedProjectile(DataOutputStream stream, short j) throws IOException {
	
		stream.writeBoolean(false);
		
		stream.writeShort( (short) updateProjs[j].getProjectile().getX() );
		stream.writeShort( (short) updateProjs[j].getProjectile().getY() );

		stream.write( updateProjs[j].getProjectile().getFrame() );
	}
	
	private void sendCD(DataOutputStream stream, short c) throws IOException {
		
		stream.write( champions[c].getPassiveCD() );
		stream.writeBoolean( champions[c].getPassiveColor() );
		stream.write( champions[c].getSpellCD() );
		stream.writeBoolean( champions[c].getSpellColor() );
		stream.write( champions[c].getUltiCD() );
		stream.writeBoolean( champions[c].getUltiColor() );
	}
	
	private void sendGates(DataOutputStream stream) throws IOException {

		for(short j=0; j<map.gates.length; j++)
			stream.writeBoolean( map.gates[j].isActivate() );
	}
	
	public int playersConnected() {
		
		short c = 0;
		for(short i=0; i<playersConnected.length; i++)
			if( playersConnected[i] )
				c++;
		
		return c;
	}
	
	public void disconnectPlayer( User u ) {
		
		for(short i=0; i<players.length; i++) {
			if(players[i] == u)
				playersConnected[i] = false;
		}
	}

	public void sendReconnectData( DataOutputStream stream, User u)  {
		
		short i;
		for( i=0; i<players.length; i++)	//search the user with the same name
			if( players[i].getUserName().equals(u.getUserName()) )
				break;
		
		try {
			
			//Tells the client he's sending this package of data
			Server.msgToClient(stream, "CreatedGameData");
			
			stream.write(i);	//tells the user his number
			
			stream.write(map.gates.length);
			
			for(short j=0; j<map.gates.length; j++) {
				stream.write((int)map.gates[j].getTilePoint().getX());
				stream.write((int)map.gates[j].getTilePoint().getY());
				stream.write(map.gates[j].getTile());
			}
			
			sendScore(stream);
			
			sendGates(stream);
			
			sendStats(stream, i);
			
			sendCD(stream, i);
		
			sendUnitsOnCreation(stream, i);
			
			sendProjectilesOnCreation(stream);
			
			u.StartConnection();
			
			u.setStatus("Game");
			
			players[i] = u;
			playersConnected[i] = true;
		
		} catch(IOException e) {
			Server.DebugLog("Error! Could not send gameCreated data to: " + players[i].getUserName() +'\n') ;
		}
	}
	
	/*public void sendPing(int c) throws IOException {
		ping[c]=(int)(System.currentTimeMillis()-pingLastCheck[c]);
		pingLastCheck[c]=System.currentTimeMillis();
		server.outDataStream[c].writeInt(ping[c]);
	}*/
	//OUTPUT TO CLIENT ***************************************************************************************************************************
	
}