package game;

import java.awt.Point;
import java.awt.Rectangle;

public class Gate {
	
	private Game game;
	private Point activatePoint;
	private Point tilePoint;
	private int tile;
	private boolean isActive;
	
	public Gate(Game game, int x, int y, int bx, int by, int tl, boolean bool) {
		this.game = game;
		activatePoint = new Point(x,y);
		tilePoint = new Point(bx,by);
		tile=tl;
		isActive=bool;
	}

	public void checkGate() {
		boolean t=true;
		for(short i=0; i<game.units.length; i++)
			if(isOnCell(activatePoint, game.units[i]) || isOnCell(tilePoint, game.units[i])) t=false;
		
		isActive=t;	
	}
	
	public boolean isOnCell(Point p, Unit u) {
		return new Rectangle((int)p.getX()*Map.cellSize,(int)p.getY()*Map.cellSize,Map.cellSize,Map.cellSize).contains(u.getX(),u.getY());
	}
	
	public Point getActivatePoint() {
		return activatePoint;
	}
	
	public Boolean isActivate() {
		return isActive;
	}
	
	public Point getTilePoint() {
		return tilePoint;
	}
	
	public int getTile() {
		return tile;
	}
	
	public void setActive(boolean a) {
		isActive=a;
	}
}
