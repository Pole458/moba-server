package game;

import projectiles.Projectile;

public class UpdateProjectile {
	
	private Projectile proj;
	private boolean created;
	
	public UpdateProjectile(Projectile p) {
		created = true;
		proj = p;
	}
	
	public boolean getCreated() {
		return created;
	}
	
	public void setCreated(boolean c) {
		created = c;
	}

	public Projectile getProjectile() {
		return proj;
	}

}
