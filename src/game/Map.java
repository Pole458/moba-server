package game;

import java.awt.Image;
import java.awt.Point;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import javax.swing.ImageIcon;

public class Map {
	
	private Game game;
	private short[][][] cells;
	public static final int cellSize=24;
	public static int tileX=52;
	public static int tileY=53;
	public Gate[] gates;
	
	public Map(Game game) {
		
		this.game = game;
		
		try {
			ObjectInputStream stream = new ObjectInputStream(new FileInputStream("map.bin"));
			cells = (short[][][])stream.readObject();
			stream.close();
			tileX=0;
			while(true)
				try {
					if(cells[0][tileX][0]<1000)	tileX++;
				}
				catch(ArrayIndexOutOfBoundsException e) {
					break;
				}
			tileY=0;
			while(true)
				try {
					if(cells[0][0][tileY]<1000) tileY++;
				}
				catch(ArrayIndexOutOfBoundsException e) {
					break;
				}
						
		} catch(ClassNotFoundException exception) {	
		} catch (IOException e) {
		}
		
		
		createTraps();
	}
	
	public Boolean getCell(int x, int y) {
		if(x>=0 && x<tileX && y>=0 && y<tileY) {
			if( cells[2][x][y]!=100 || getTrap(x,y) ) return false;
			return true;
		}
		return false;
	}
	
	public Boolean getTrap(int x, int y) {
		Point p = new Point(x,y);
		for(int i=0; gates.length>i; i++)
			if(gates[i].getTilePoint().equals(p) && gates[i].isActivate()) return true;
		return false;
	}
	
	public Image drawCell(int l, int x, int y) {
		if(l>=0 && l<4 && x>=0 && x<tileX && y>=0 && y<tileY)
			return new ImageIcon(("TileSet/tile_"+cells[l][x][y]+".png")).getImage();
		else return null;
	}
	
	private void createTraps() {
		
		gates = new Gate[2];
		
		gates[0] = new Gate(game, 7, 26, 15, 23, 168, true);
		gates[1] = new Gate(game, 34, 18, 26, 21, 168, true);
	}
}
