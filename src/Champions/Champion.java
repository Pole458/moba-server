package Champions;
import game.Game;
import game.Unit;

public class Champion extends Unit {
	protected boolean upKey;
	protected boolean downKey;
	protected boolean leftKey;
	protected boolean rightKey;
	protected boolean JKey;
	protected boolean KKey;
	protected boolean LKey;
	protected boolean visible;
    protected short passiveCD;
    protected boolean passiveColor;
    protected short spellCD;
    protected boolean spellColor;
    protected short ultiCD;
    protected boolean ultiColor;
    protected String bonus;
    
    public Champion( Game game ) {
    	
    	super(game);
    	
    	upKey=false;
    	downKey=false;
    	leftKey=false;
    	rightKey=false;
    	JKey=false;
    	KKey=false;
    	LKey=false;
    	frameIndex=0;
    	spriteIndex=0;
    	status="";
    	visible=true;
    	bonus="Fly";
    	
    	resetBonus();
    	
    }
    
    public Champion(Game game, String name, boolean team) {
    	
    	this(game);
    	
    	this.name=name;
    	
    	if(team) this.team = 'B';
    	else this.team = 'R';
    	
    	if(this.team=='B') {
    		x=7*24;
    		y=8*24;
    	}
    	if(this.team=='R') {
    		x=34*24;
    		y=35*24;
    	}
    }

    /*public Champion(Champion p) {
    	
    	super(p);
    	
    	this.upKey=p.upKey;
    	this.downKey=p.downKey;
      	this.leftKey=p.leftKey;
    	this.rightKey=p.rightKey;
    	this.JKey=p.JKey;
    	this.KKey=p.KKey;
    	this.LKey=p.LKey;
        this.visible=p.visible;
        this.passiveCD=p.passiveCD;
        this.passiveColor=p.passiveColor;
        this.spellCD=p.spellCD;
        this.spellColor=p.spellColor;
        this.ultiCD=p.ultiCD;
        this.ultiColor=p.ultiColor;
        this.bonus=p.bonus;
    }*/
    
    public void update() {
    	
    	if(isDeath()) {
    		status="death";
    		statusTimer=5000;
    		spriteIndex=48;
    		frameIndex=0;
    		statusLastCast=System.currentTimeMillis();
    	}
    	
    	switch(status) {
    	case "":
    		direction();
    		break;
    	case "stun":
       		hSpeed=0;
    		vSpeed=0;
    		if(statusTimer+statusLastCast<System.currentTimeMillis()) status="";
    		break;
    	case "knockBack":
    		vSpeed=-2*Math.sin(direction*Math.PI/180);
			hSpeed=2*Math.cos(direction*Math.PI/180);
			if(statusTimer+statusLastCast<System.currentTimeMillis()) status="";
    		break;
    	case "death":
    		HP=0.5;
    		hSpeed=0;
    		vSpeed=0;
    		if(frameIndex<50) frameIndex+=1.2;
    		if(statusTimer+statusLastCast<System.currentTimeMillis()) {
    			HP=getHPMax();
    			status="";
    			if(this.team=='B') {
    	    		x=7*24;
    	    		y=8*24;
    	    	}
    	    	if(this.team=='R') {
    	    		x=34*24;
    	    		y=35*24;
    	    	}
    		}
    		break;
    	}
    	
    	framing();
    	wallContact();
    	
    	x+=hSpeed;
    	y+=vSpeed;
    	
    	super.update();
    }
    
    public void wallContact() {
    	
    	switch(getDirection45(direction)) {
    	case 0:
            if(!game.map.getCell((int)(x+width)/24,(int)(y-width/2)/24)) {
                hSpeed=0;
            }        
    	break;
    	case 45:
    		if(!game.map.getCell((int)(x+width)/24,(int)(y-width)/24)) {
    			hSpeed=0;
    			vSpeed=0;
    		}        
		break;
    	case 90:
    		if(!game.map.getCell((int)(x/24),(int)(y-width)/24)) {
    			vSpeed=0;
    		}        
    	break;
    	case 135:
    		if(!game.map.getCell((int)(x-width)/24,(int)(y-width)/24)) {
    			hSpeed=0;
    			vSpeed=0;
    		}        
    	break;
    	case 180:
    		if(!game.map.getCell((int)(x-width)/24,(int)(y/24))) {
    			hSpeed=0;
    		}        
    	break;
    	case 225:
    		if(!game.map.getCell((int)(x-width)/24,(int)(y+width)/24)) {
    			hSpeed=0;
    			vSpeed=0;
    		}        
    	break;
    	case 270:
    		if(!game.map.getCell((int)(x/24),(int)(y+width)/24)) {
    			vSpeed=0;
    		}        
    	break;
    	case 315:
    		if(!game.map.getCell((int)(x+width)/24,(int)(y+width)/24)) {
    			hSpeed=0;
    			vSpeed=0;
    		}        
		break;
    	}
    }
  
    public void direction() {
    	if(status.equals("")) {
    		if(upKey) {
        		direction=90;
        		if(leftKey) direction=135;
        		if(rightKey) direction=45;
          	}
        	else if(downKey) {
        		direction=270;
        		if(leftKey) direction=225;
        		if(rightKey) direction=315;
        	}
        	else {
        		if(leftKey) direction=180;
        		if(rightKey) direction=0;
        	}
        	if(upKey || downKey || leftKey || rightKey) {
        		vSpeed=-getSpeed()*Math.sin(direction*Math.PI/180);
    			hSpeed=getSpeed()*Math.cos(direction*Math.PI/180);
        	}
        	else {
        		vSpeed=0;
        		hSpeed=0;
        	}
    	}
    }
    
    public void framing() {
    	if(status.equals("")) {
    		if(frameIndex<50) frameIndex+=1.2;
        	else frameIndex=0;
        	spriteIndex=0;
        	switch(direction) {
        	case 0:
        		spriteIndex=6;
        		break;
        	case 45:
        		spriteIndex=18;
        		break;
        	case 90:
        		spriteIndex=18;
        		break;
        	case 135:
        		spriteIndex=12;
        		break;
        	case 180:
        		spriteIndex=0;
        		break;
        	case 225:
        		spriteIndex=0;
        		break;
        	case 270:
        		spriteIndex=0;
        		break;
        	case 315:
        		spriteIndex=6;
        		break;
        	}
        	if(vSpeed!=0 || hSpeed!=0) spriteIndex+=24;
    	}
    }

    public void resetPosition() {
    	if(this.team=='B') {
    		x=7*24;
    		y=8*24;
    	}
    	if(this.team=='R') {
    		x=34*24;
    		y=35*24;
    	}
    }
    
    public void onHit(Unit target) {
    	
    }
    
    public void spellHit(Unit target) {
    	
    }
    
    public void applyLifeSteal(double damageDealt) {
    	HP=Math.min(getHP()+damageDealt*getLifeSteal()/100, getHPMax());
	}
    
    public static boolean isVisible(Unit u) {
		return u.getVisible();
	}
    
    public void updateCD() {
    	passiveCD=0;
    	passiveColor=false;
    	spellCD=0;
    	passiveColor=false;
    	ultiCD=0;
    	ultiColor=false;
    }
    
    public short getDirection() {
    	return direction;
    }
    public boolean getVisible() {
    	return visible;
    }
    public short getPassiveCD() {
    	return passiveCD;
    }
    public boolean getPassiveColor() {
    	return passiveColor;
    }
    public short getSpellCD() {
    	return spellCD;
    }
    public boolean getSpellColor() {
    	return spellColor;
    }
    public short getUltiCD() {
    	return ultiCD;
    }
    public boolean getUltiColor() {
    	return ultiColor;
    }
    public String getBonus() {
    	return bonus;
    }
    
    public boolean getJKey() {
    	return JKey;
    }
    public boolean getKKey() {
    	return KKey;
    }
    public boolean getLKey() {
    	return LKey;
    }
    
    public void setUpKey(boolean b) {
    	upKey=b;
    }
    public void setDownKey(boolean b) {
    	downKey=b;
    }
    public void setLeftKey(boolean b) {
    	leftKey=b;
    }
    public void setRightKey(boolean b) {
    	rightKey=b;
    }
    public void setJKey(boolean b) {
    	JKey=b;
    }
    public void setKKey(boolean b) {
    	KKey=b;
    }
    public void setLKey(boolean b) {
    	LKey=b;
    }
}
