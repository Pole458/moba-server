package Champions;

import game.Game;
import game.Unit;
import projectiles.AoE;
import projectiles.Mark;

public class Purprowl extends Champion {
	private short passiveRange;
	private Unit passiveTarget;
	private double passiveDamage;
	private short passiveCadence;
	private long passiveLastCast;
	private Mark passiveMark;

	private short spellTimer;
	private long spellLastCast;
	private double spellDamage;
	private short spellRange;
	private AoE spellAoE;
	
	private int ultiTimer;
	private long ultiLastCast;
	private double ultiDamage;
	private Unit ultiTarget;
	
	public Purprowl(Game game, String name, boolean team) {
		
		super(game, name, team);
		
		sprite = 2;
		width = 10;
		downDistance = 55;
		upDistance = 15;
		
		HPMax=220;
		armor=20;
		damage=50;
		attTimer=1660;
		lifesteal=40;
		speed=1;
		range=35;
				
		passiveRange=120;
		passiveTarget=null;
		passiveDamage=0.1;
		passiveCadence=1000;
		passiveMark = new Mark(this,this,(short)5,0,(short)16,(short)16);
		game.addProjectile(passiveMark);
		
		spellTimer=10000;
		spellDamage=2;
		spellRange=60;
		
		ultiTimer=40000;
		ultiDamage=2;
		ultiTarget=null;
		
		HP=HPMax;
		attLastCast=System.currentTimeMillis();
		passiveLastCast=System.currentTimeMillis();
		spellLastCast=System.currentTimeMillis();
		ultiLastCast=System.currentTimeMillis();
	}
	
	public void update() {
		
		if(isDeath()) {
			
			passiveMark.setTarget(this);
			//passiveMark.setSprite((short)-1);
		
		} else if(passiveCadence+passiveLastCast<System.currentTimeMillis()) {
			
			passiveLastCast = System.currentTimeMillis();
			passiveTarget = weakestEnemy();
			
			if(passiveTarget != null) {
				passiveMark.setTarget(passiveTarget);
				//passiveMark.setSprite((short)5);
			} else {
				passiveMark.setTarget(this);
				//passiveMark.setSprite((short)-1);
			}
		}
		
		if(KKey && getAttTimer()+attLastCast<System.currentTimeMillis() && status.equals("")) {
			target=nearestEnemyUnit();
			if(target!=null)
				if(distanceToUnit(target)<getRange()) {
					attLastCast=System.currentTimeMillis();
					status="attack";
					frameIndex=0;
				}
				else target=null;
		}
		
		if(JKey && spellTimer+spellLastCast<System.currentTimeMillis() && status.equals("")) {
			spellLastCast=System.currentTimeMillis();
			status="spell";
			frameIndex=0;
		}
		
		if(LKey && ultiTimer+ultiLastCast<System.currentTimeMillis() && status.equals("") && passiveTarget!=null) {
			ultiTarget=passiveTarget;
			ultiLastCast=System.currentTimeMillis();
			status="ulti";
			frameIndex=0;
		}
		
		switch(status) {
		case "attack":
			if(target!=null) {
				hSpeed=0;
				vSpeed=0;
				direction=getPointDirection45(target.getX(),target.getY());
				switch(direction) {
				case 0:
					spriteIndex=60;
					break;
				case 45:
					spriteIndex=72;
					break;
				case 90:
					spriteIndex=72;
					break;
				case 135:
					spriteIndex=66;
					break;
				case 180:
					spriteIndex=54;
					break;
				case 225:
					spriteIndex=54;
					break;
				case 270:
					spriteIndex=54;
					break;
				case 315:
					spriteIndex=60;
					break;
				}
				
				if(frameIndex>35 && frameIndex<38) {
					damaging(target,getDamage(),true,false);
					frameIndex=38;
				}
				
				if(frameIndex<50) frameIndex+=1500/(double)getAttTimer();
				else {
					status="";
				}
			}
			break;
		case "spell":
			if(frameIndex==0) {
				spellAoE = new AoE(getX(),getY(),spellRange,this);
				game.addProjectile(spellAoE);
			}
			hSpeed=0;
			vSpeed=0;
			switch(getPointDirection45(getX(),getY())) {
			case 0:
				spriteIndex=60;
				break;
			case 45:
				spriteIndex=72;
				break;
			case 90:
				spriteIndex=72;
				break;
			case 135:
				spriteIndex=66;
				break;
			case 180:
				spriteIndex=54;
				break;
			case 225:
				spriteIndex=54;
				break;
			case 270:
				spriteIndex=54;
				break;
			case 315:
				spriteIndex=60;
				break;
			}
			
			if(frameIndex>35 && frameIndex<38) {
				Unit[] enemyHit = getEnemyAoE(getX(),getY(),spellRange);
				for(int i=0; i<enemyHit.length;i++)
					damaging(enemyHit[i],getDamage()*spellDamage/enemyHit.length,false,true);
				frameIndex=38;
			}
			
			if(frameIndex<50) frameIndex+=1.2;
			else {
				status="";
				spellAoE.destroy();
				spellAoE = null;
			}
			break;
		case "ulti":
			hSpeed=0;
			vSpeed=0;
			direction=getPointDirection45(ultiTarget.getX(),ultiTarget.getY());
			switch(direction) {
			case 0:
				spriteIndex=90;
				break;
			case 45:
				spriteIndex=114;
				break;
			case 90:
				spriteIndex=114;
				break;
			case 135:
				spriteIndex=102;
				break;
			case 180:
				spriteIndex=78;
				break;
			case 225:
				spriteIndex=78;
				break;
			case 270:
				spriteIndex=78;
				break;
			case 315:
				spriteIndex=90;
				break;
			}
			
			if(frameIndex>65 && frameIndex<68) {
				x=ultiTarget.getX();
				y=ultiTarget.getY();
				damaging(ultiTarget,getDamage()*ultiDamage,false,false);
				frameIndex=68;
			}
			
			if(frameIndex<110) frameIndex+=1.2;
			else {
				status="";
			}
			break;
		}
		updateCD();
		super.update();
	}
	
	public void onHit(Unit target) {
		if(passiveMark.getTarget().equals(target))
			damaging(target,getDamage()*passiveDamage,false,false);
		super.onHit(target);
	}
	
	public Unit weakestEnemy() {
		int weakest=0;
		while(weakest<game.units.length && (!isTargettable(game.units[weakest]) || distanceToUnit(game.units[weakest])>passiveRange || !game.units[weakest].isAChampion()) )
			weakest++;
		
		int next=weakest+1;
		while(next<game.units.length && (!isTargettable(game.units[next]) ||  distanceToUnit(game.units[next])>passiveRange || !game.units[next].isAChampion()) )
			next++;
		
		while(next<game.units.length) {
			if(game.units[next].getHP()*game.units[next].getArmor()<=game.units[weakest].getHP()*game.units[weakest].getArmor())
				weakest=next;
				next++;
			while(next<game.units.length && (!isTargettable(game.units[next]) || distanceToUnit(game.units[next])>passiveRange || !game.units[next].isAChampion()) )
				next++;
		}
		
		if(weakest<game.units.length) return game.units[weakest];
		else return null;
	}
	
	public void updateCD() {
		passiveCD=100;
		if(passiveTarget!=null) passiveColor=true;
		else passiveColor=false;
		spellCD=(short)Math.min(100*(System.currentTimeMillis()-spellLastCast)/spellTimer, 100);
		if(spellCD==100) spellColor=true;
		else spellColor=false;
		ultiCD=(short)Math.min(100*(System.currentTimeMillis()-ultiLastCast)/ultiTimer, 100);
		if(ultiCD==100 && passiveTarget!=null) ultiColor=true;
		else ultiColor=false;
		
	}
}
