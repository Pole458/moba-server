package Champions;

import projectiles.AoE;
import game.Game;
import game.Unit;

public class Steamstoove extends Champion {
	private double vapor;
	
	private AoE attackAoE;

	private short spellTimer;
	private long spellLastCast;
	private double spellDamage;
	private short spellDuration;
	private Unit[] spellEnemyTarget;
	private Unit[] spellAllyTarget;
	private short spellVapor;
	private short spellRange;
	private AoE spellAoE;
	
	private int ultiTimer;
	private long ultiLastCast;
	private double ultiDamage;
	private short ultiRange;
	private AoE ultiAoE;
	
	public Steamstoove(Game game, String name, boolean team) {
		
		super(game, name,team);
		
		sprite = 7;
		width = 10;
		
		downDistance = 45;
		upDistance = 5;
		
		HPMax=310;
		armor=40;
		damage=30;
		attTimer=1660;
		lifesteal=12;
		speed=0.7;
		range=50;
				
		vapor=0;
		
		attackAoE=null;
		
		spellTimer=8000;
		spellDamage=0.05;
		spellDuration=3000;
		spellVapor=0;
		spellEnemyTarget=null;
		spellAllyTarget=null;
		spellRange=60;
		spellAoE=null;
		
		ultiTimer=45000;
		ultiDamage=0.1;
		ultiRange=70;
		ultiAoE=null;
		
		HP=HPMax;
		attLastCast=System.currentTimeMillis();
		spellLastCast=System.currentTimeMillis();
		ultiLastCast=System.currentTimeMillis();
	}
	
	public void update() {
		if(hSpeed!=0 || vSpeed!=0) {
			vapor=Math.min(vapor+=0.4, 100);
		} else {
			if(status.equals("")) vapor=Math.max(0,vapor-=0.4);
		}
		
		if(status.equals("stun") || status.equals("knockBack") ) {
			vapor=0;
		}
		
		newSpeedBonus((int)vapor/2);
		newArmorBonus((int)vapor/2);
		
		if(spellDuration+spellLastCast>System.currentTimeMillis()) {
			if(spellEnemyTarget!=null)
				for(short i=0; i<spellEnemyTarget.length; i++)
					spellEnemyTarget[i].newSpeedBonus(-Math.min(75,spellVapor));
			if(spellEnemyTarget!=null)
				for(short i=0; i<spellAllyTarget.length; i++)
					spellAllyTarget[i].newSpeedBonus(spellVapor);
		}
		
		
		if(KKey && getAttTimer()+attLastCast<System.currentTimeMillis() && status.equals("")) {
			target=nearestEnemyUnit();
			if(target!=null)
				if(distanceToUnit(target)<getRange()) {
					attLastCast=System.currentTimeMillis();
					status="attack";
					frameIndex=0;
				}
				else target=null;
		}
		
		if(JKey && spellTimer+spellLastCast<System.currentTimeMillis() && status.equals("")) {
			spellLastCast=System.currentTimeMillis();
			status="spell";
			frameIndex=0;
		}
		
		if(LKey && ultiTimer+ultiLastCast<System.currentTimeMillis() && status.equals("") && vapor==100) {
			ultiLastCast=System.currentTimeMillis();
			status="ulti";
			frameIndex=0;
		}
		
		switch(status) {
		case "attack":
			if(frameIndex==0) {
				attackAoE = new AoE(getX(),getY(),getRange(),this);
				game.addProjectile(attackAoE);
			}
			if(target!=null) {
				hSpeed=0;
				vSpeed=0;
				direction=getPointDirection45(target.getX(),target.getY());
				switch(direction) {
				case 0:
					spriteIndex=60;
					break;
				case 45:
					spriteIndex=72;
					break;
				case 90:
					spriteIndex=72;
					break;
				case 135:
					spriteIndex=66;
					break;
				case 180:
					spriteIndex=54;
					break;
				case 225:
					spriteIndex=54;
					break;
				case 270:
					spriteIndex=54;
					break;
				case 315:
					spriteIndex=60;
					break;
				}
				
				if(frameIndex>35 && frameIndex<38) {
					applyLifeSteal(damagingAoE(getX(),getY(),getRange(),getDamage(),false,false));
					vapor+=10*getEnemyAoE(getX(),getY(),getRange()).length;
					frameIndex=38;
				}
				
				if(frameIndex<50) frameIndex+=1500/(double)getAttTimer();
				else {
					status="";
					attackAoE.destroy();
					attackAoE=null;
				}
			}
			break;
		case "spell":
			hSpeed=0;
			vSpeed=0;
			if(frameIndex==0) {
				spellAoE = new AoE(getX(),getY(),spellRange,this);
				game.addProjectile(spellAoE);
			}
			spellAoE.setX(getX());
			spellAoE.setY(getY());
			switch(getDirection45(direction)) {
			case 0:
				spriteIndex=60;
				break;
			case 45:
				spriteIndex=72;
				break;
			case 90:
				spriteIndex=72;
				break;
			case 135:
				spriteIndex=66;
				break;
			case 180:
				spriteIndex=54;
				break;
			case 225:
				spriteIndex=54;
				break;
			case 270:
				spriteIndex=54;
				break;
			case 315:
				spriteIndex=60;
				break;
			}
			
			if(frameIndex<50) frameIndex+=1.2;
			else {
				if(vapor>75) {
					damagingAoE(getX(),getY(),60,2*getHPMax()*spellDamage,false,true);
				}
				else{
					damagingAoE(getX(),getY(),60,getHPMax()*spellDamage,false,true);
				}
				
				spellEnemyTarget=getEnemyAoE(getX(),getY(),spellRange);
				spellAllyTarget=getAllyAoE(getX(),getY(),spellRange);
				spellVapor=(short)vapor;
				vapor=0;
				status="";
				
				spellAoE.destroy();
				spellAoE=null;
			}
			break;
		case "ulti":
			if(frameIndex==0) {
				ultiAoE = new AoE(getX(),getY(),ultiRange,this);
				game.addProjectile(ultiAoE);
			}
			hSpeed=0;
			vSpeed=0;
			if(direction>=180 && direction<=270) spriteIndex=78;
			else spriteIndex=84;
			
			if(frameIndex>35 && frameIndex<38) {
				vapor=0;
				
				damagingAoE(getX(),getY(),ultiRange,getHPMax()*ultiDamage,false,true);
				Unit[] enemyHit = getEnemyAoE(getX(),getY(),ultiRange);
				for(short i=0; i<enemyHit.length; i++) {
					enemyHit[i].setStatus("knockBack",(short)250);
					enemyHit[i].setDirection(enemyHit[i].getPointDirection(getX(), getY()));
					vapor=Math.min(vapor+10, 100);
				}
				frameIndex=38;
			}
			if(frameIndex<50) frameIndex++;
			else {
				status="";
				ultiAoE.destroy();
				ultiAoE=null;
			}
			break;
		}
		
		updateCD();
		super.update();
	}
	
	public void updateCD() {
		passiveCD=(short)vapor;
		if(vapor>75) passiveColor=true;
		else passiveColor=false;
		spellCD=(short)Math.min(100*(System.currentTimeMillis()-spellLastCast)/spellTimer, 100);
		if(spellCD==100 && visible) spellColor=true;
		else spellColor=false;
		ultiCD=(short)Math.min(100*(System.currentTimeMillis()-ultiLastCast)/ultiTimer, 100);
		if(ultiCD==100 && vapor==100) ultiColor=true;
		else ultiColor=false;
	}
}