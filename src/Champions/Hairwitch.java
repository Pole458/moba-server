package Champions;

import game.Game;
import game.Unit;
import projectiles.AoE;
import projectiles.BouncingProjectile;
import projectiles.Mark;
import projectiles.TargetProjectile;

public class Hairwitch extends Champion {
	
	private short passiveStack;
	private double passiveDamage;
	private short passiveRange;
	
	private Unit spellTarget;
	private short spellTimer;
	private long spellLastCast;
	private double spellDamage;
	private short spellRange;
	private short spellBounce;
	private short spellBounceRange;
	private BouncingProjectile spellProj;
	private AoE spellAoE;
	
	private long ultiLastCast;
	private int ultiTimer;
	private long ultiLastActivation;
	private short ultiDuration;
	private short ultiRange;
	private short ultiDamageRange;
	private double ultiDamage;
	private Mark ultiMark;
	private Unit ultiTarget;
	private AoE ultiAoE;
	
	public Hairwitch(Game game, String name, boolean team) {
		
		super(game, name, team);
		
		sprite=4;
		width=10;
		
		downDistance = 47;
		upDistance = 5;
		
		HPMax=270;
		armor=8;
		damage=40;
		attTimer=1660;
		lifesteal=20;
		speed=0.7;
		range=110;
		
		passiveRange=50;
		passiveStack=0;
		passiveDamage=0.03;
		
		spellTimer=10000;
		spellRange=110;
		spellDamage=1;
		spellBounce=4;
		spellBounceRange=50;
		spellProj=null;
		spellAoE=null;
		
		ultiTimer=60000;
		ultiDuration=5000;
		ultiRange=110;
		ultiDamageRange=50;
		ultiDamage=0.05;
		ultiAoE=null;
		ultiMark=null;
		
		HP=HPMax;
		attLastCast=System.currentTimeMillis();
		spellLastCast=System.currentTimeMillis();
		ultiLastCast=System.currentTimeMillis();
		ultiLastActivation=System.currentTimeMillis();
	}
	
	public void update() {
		
		if(spellProj!=null) {
			if(spellAoE==null) {
				spellAoE = new AoE(spellProj.getX(),spellProj.getY(),spellBounceRange,this);
				game.addProjectile(spellAoE);
			}
			else {
				spellAoE.setX(spellProj.getX());
				spellAoE.setY(spellProj.getY());
			}
			if(spellProj.isDestroyed()) spellProj=null;
		} else if(spellAoE!=null) {
			spellAoE.destroy();
			spellAoE=null;
		}
		
		if(ultiMark!=null) {
			if(ultiAoE==null) {
				ultiAoE = new AoE(ultiMark.getTarget().getX(),ultiMark.getTarget().getY(),ultiDamageRange,this);
				game.addProjectile(ultiAoE);
			} else {
				ultiAoE.setX(ultiMark.getTarget().getX());
				ultiAoE.setY(ultiMark.getTarget().getY());
			}
			if(ultiDuration+ultiLastCast>System.currentTimeMillis()) {
				if(500+ultiLastActivation<System.currentTimeMillis()) {
					damagingAoE(ultiMark.getTarget().getX(),ultiMark.getTarget().getY(),ultiDamageRange,getHPMax()*ultiDamage,false,true);
					ultiLastActivation=System.currentTimeMillis();
				}
			} else {
				ultiMark.destroy();
				ultiMark=null;
				ultiAoE.destroy();
				ultiAoE=null;
			}
		}
		
		if(KKey && getAttTimer()+attLastCast<System.currentTimeMillis() && status.equals("")) {
			target=nearestEnemyUnit();
			if(target!=null)
				if(distanceToUnit(target)<getRange()) {
					attLastCast=System.currentTimeMillis();
					status="attack";
					frameIndex=0;
				}
				else target=null;
		}
		
		if(JKey && spellTimer+spellLastCast<System.currentTimeMillis() && status.equals("")) {
			spellTarget=nearestEnemyUnit();
			if(spellTarget!=null)
				if(distanceToUnit(spellTarget)<spellRange) {
					spellLastCast=System.currentTimeMillis();
					status="spell";
					frameIndex=0;
				}
				else spellTarget=null;
		}
		
		if(LKey && ultiTimer+ultiLastCast<System.currentTimeMillis() && status.equals("")) {
			ultiTarget=nearestUnit();
			if(ultiTarget!=null)
				if(distanceToUnit(ultiTarget)<ultiRange) {
					ultiLastCast=System.currentTimeMillis();
					status="ulti";
					frameIndex=0;
				}
				else ultiTarget=null;
		}
		
		switch(status) {
		case "attack":
			if(target!=null) {
				hSpeed=0;
				vSpeed=0;
				direction=getPointDirection45(target.getX(),target.getY());
				switch(direction) {
				case 0:
					spriteIndex=60;
					break;
				case 45:
					spriteIndex=72;
					break;
				case 90:
					spriteIndex=72;
					break;
				case 135:
					spriteIndex=66;
					break;
				case 180:
					spriteIndex=54;
					break;
				case 225:
					spriteIndex=54;
					break;
				case 270:
					spriteIndex=54;
					break;
				case 315:
					spriteIndex=60;
					break;
				}
				
				if(frameIndex>35 && frameIndex<38) {
					game.addProjectile(new TargetProjectile(x,y,5,target,(short)3,(short)10,(short)10,getDamage(),this,true,false));
					frameIndex=38;
				}
				
				if(frameIndex<59) frameIndex+=1500/(double)getAttTimer();
				else {
					status="";
				}
			}
			break;
		case "spell":
			if(spellTarget!=null) {
				hSpeed=0;
				vSpeed=0;
				direction=getPointDirection45(spellTarget.getX(),spellTarget.getY());
				switch(direction) {
				case 0:
					spriteIndex=60;
					break;
				case 45:
					spriteIndex=72;
					break;
				case 90:
					spriteIndex=72;
					break;
				case 135:
					spriteIndex=66;
					break;
				case 180:
					spriteIndex=54;
					break;
				case 225:
					spriteIndex=54;
					break;
				case 270:
					spriteIndex=54;
					break;
				case 315:
					spriteIndex=60;
					break;
				}
				
				if(frameIndex>35 && frameIndex<38) {
					spellProj = new BouncingProjectile(x,y,3,spellTarget,(short)3,(short)10,(short)10,getDamage()*spellDamage,this,false,true,spellBounce,spellBounceRange);
					game.addProjectile(spellProj);
					frameIndex=38;
				}
				
				if(frameIndex<50) frameIndex+=1.2;
				else {
					status="";
					spellTarget=null;
				}
			}
			break;
		case "ulti":
			hSpeed=0;
			vSpeed=0;
			switch(getDirection45(direction)) {
			case 0:
				spriteIndex=60;
				break;
			case 45:
				spriteIndex=72;
				break;
			case 90:
				spriteIndex=72;
				break;
			case 135:
				spriteIndex=66;
				break;
			case 180:
				spriteIndex=54;
				break;
			case 225:
				spriteIndex=54;
				break;
			case 270:
				spriteIndex=54;
				break;
			case 315:
				spriteIndex=60;
				break;
			}
			
			if(frameIndex>35 && frameIndex<38) {
				ultiMark = new Mark(ultiTarget,this,(short)3,0,(short)10,(short)10);
				game.addProjectile(ultiMark);
				ultiLastCast=System.currentTimeMillis();
				frameIndex=38;
			}
			
			if(frameIndex<50) frameIndex+=1.2;
			else {
				status="";
			}
		}
		updateCD();
		super.update();
	}
	
	public void onHit(Unit target) {
    	if(passiveStack>0) {
    		damagingAoE(target.getX(),target.getY(),passiveRange,passiveStack*getHPMax()*passiveDamage,false,false);
    		passiveStack=0;
    	}
    	
    	super.onHit(target);
    }
	
	public void spellHit(Unit target) {
		passiveStack=(short)Math.min(10,passiveStack+1);
	}
	
    public void updateCD() {
		passiveCD=(short)Math.min(100*(passiveStack)/10, 100);
		if(passiveCD==100) passiveColor=true;
		else passiveColor=false;
		spellCD=(short)Math.min(100*(System.currentTimeMillis()-spellLastCast)/spellTimer, 100);
		if(spellCD==100) spellColor=true;
		else spellColor=false;
		ultiCD=(short)Math.min(100*(System.currentTimeMillis()-ultiLastCast)/ultiTimer, 100);
		if(ultiCD==100) ultiColor=true;
		else ultiColor=false;
	}
}
