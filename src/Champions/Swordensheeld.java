package Champions;

import game.Game;
import game.Unit;
import projectiles.AoE;

public class Swordensheeld extends Champion {
	private double stamina;
	
	private boolean shieldUp;
	
	private short ultiTimer;
	private long ultiLastCast;
	private double ultiDamage;
	private short ultiRange;
	private AoE ultiAoE;
	private Unit[] enemySlowed;
	private short slowTimer;
	private long slowActivation;
	private short ultiSlow;
	
	public Swordensheeld(Game game, String name, boolean team) {
		
		super(game, name, team);
		
		sprite = 8;
		width = 10;
		
		downDistance = 40;
		upDistance = 5;
		
		HPMax=240;
		armor=40;
		damage=50;
		attTimer=1660;
		lifesteal=15;
		speed=0.7;
		range=35;
		
		stamina=0;
				
		ultiTimer=5000;
		ultiRange=60;
		ultiDamage=1;
		ultiAoE=null;
		enemySlowed=null;
		slowTimer=2000;
		ultiSlow=50;
		
		HP=HPMax;
		attLastCast=System.currentTimeMillis();
		ultiLastCast=System.currentTimeMillis();
		slowActivation=System.currentTimeMillis();
	}
	
	public void update() {
		
		if(JKey && status.equals("") && stamina>=0) {
			shieldUp=true;
		}
		else shieldUp=false;
		
		if(!shieldUp && !status.equals("ulti"))	stamina=Math.min(stamina+0.4,100);
		if(shieldUp) newArmorBonus((short)stamina);
		
		if(enemySlowed!=null)
			if(slowTimer+slowActivation>System.currentTimeMillis()) {
				for(short i=0; i<enemySlowed.length; i++)
					enemySlowed[i].newSpeedBonus(-ultiSlow);
			} else enemySlowed=null;
		
		if(KKey && getAttTimer()+attLastCast<System.currentTimeMillis() && status.equals("") && !shieldUp) {
			target=nearestEnemyUnit();
			if(target!=null)
				if(distanceToUnit(target)<getRange()) {
					attLastCast=System.currentTimeMillis();
					status="attack";
					frameIndex=0;
				}
				else target=null;
		}
		
		
		
		if(LKey && ultiTimer+ultiLastCast<System.currentTimeMillis() && status.equals("") && !shieldUp) {
			ultiLastCast=System.currentTimeMillis();
			status="ulti";
			frameIndex=0;
		}
		
		switch(status) {
		case "attack":
			if(target!=null) {
				hSpeed=0;
				vSpeed=0;
				direction=getPointDirection45(target.getX(),target.getY());
				switch(direction) {
				case 0:
					spriteIndex=60;
					break;
				case 45:
					spriteIndex=72;
					break;
				case 90:
					spriteIndex=72;
					break;
				case 135:
					spriteIndex=66;
					break;
				case 180:
					spriteIndex=54;
					break;
				case 225:
					spriteIndex=54;
					break;
				case 270:
					spriteIndex=54;
					break;
				case 315:
					spriteIndex=60;
					break;
				}
				
				if(frameIndex>35 && frameIndex<38) {
					damaging(target,getDamage(),true,false);
					frameIndex=38;
				}
				
				if(frameIndex<50) frameIndex+=1500/(double)getAttTimer();
				else status="";
				
			}
			break;
		case "ulti":
			if(frameIndex==0) {
				ultiAoE = new AoE(getX(),getY(),ultiRange,this);
				game.addProjectile(ultiAoE);
			}
			switch(getDirection45(direction)) {
			case 0:
				spriteIndex=60;
				break;
			case 45:
				spriteIndex=72;
				break;
			case 90:
				spriteIndex=72;
				break;
			case 135:
				spriteIndex=66;
				break;
			case 180:
				spriteIndex=54;
				break;
			case 225:
				spriteIndex=54;
				break;
			case 270:
				spriteIndex=54;
				break;
			case 315:
				spriteIndex=60;
				break;
			}
			hSpeed=0;
			vSpeed=0;
			
			if(frameIndex>15 && frameIndex<18 && LKey) {
				frameIndex=15;
				stamina=Math.max(stamina-1,0);
				ultiDamage=Math.min(ultiDamage+0.01,2);
				if(ultiDamage>=2 || stamina==0) frameIndex=18;
			}
			
			if(frameIndex>35 && frameIndex<38) {
				damagingAoE(getX(),getY(),ultiRange,getDamage()*ultiDamage,false,false);
				Unit[] enemyHit = getEnemyAoE(getX(),getY(),ultiRange);
				for(short i=0; i<enemyHit.length; i++) {
					enemyHit[i].setStatus("stun",(short)(1000*ultiDamage));
				}
				frameIndex=38;
			}
			
			if(frameIndex<50) frameIndex+=1.2;
			else {
				status="";
				ultiAoE.destroy();
				ultiAoE = null;
				ultiDamage=1;
			}
			
			break;
		}
		
		updateCD();
		super.update();
	}
	
	public void framing() {
    	super.framing();
    	if(shieldUp && !status.equals("death")) spriteIndex+=78;
    }
	
	public void setHP(double hp) {
		if(shieldUp) {
			stamina=Math.max(0,stamina-(getHP()-hp)*100/getHPMax());
		}
		super.setHP(hp);
	}
	
	public void updateCD() {
		if(!shieldUp && !status.equals("ulti")) passiveColor=true;
		else passiveColor=false;
		passiveCD=(short)stamina;
		spellCD=100;
		if(shieldUp) spellColor=true;
		else spellColor=false;
		ultiCD=(short)Math.min(100*(System.currentTimeMillis()-ultiLastCast)/ultiTimer, 100);
		if(ultiCD==100) ultiColor=true;
		else ultiColor=false;
	}
}