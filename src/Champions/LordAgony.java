package Champions;

import game.Game;
import projectiles.AoE;

public class LordAgony extends Champion {
	private double passiveDamage;
	private long passiveLastActivation;
	private short passiveDuration;
	private long passiveLastCast;
	private int passiveTimer;
	private short passiveRange;
	private AoE passiveAoE;
	
	private short spellTimer;
	private long spellLastCast;
	private long spellLastActivation;
	private double spellHPCost;
	private boolean spellActive;
	
	private int ultiTimer;
	private long ultiLastCast;
	private short ultiBonusDuration;
	private short ultiRange;
	private AoE ultiAoE;
	
	public LordAgony(Game game, String name, boolean team) {
		
		super(game, name, team);
		
		sprite = 5;
		width = 10;
		
		downDistance = 43;
		upDistance = 3;
		
		HPMax=220;
		armor=40;
		damage=26;
		attTimer=1330;
		lifesteal=60;
		speed=0.75;
		range=35;
				
		passiveTimer=60000;
		passiveDamage=0.5;
		passiveDuration=2000;
		passiveRange=50;
		passiveAoE=null;
		
		spellTimer=4000;
		spellHPCost=0.025;
		spellActive=false;
		
		ultiTimer=30000;
		ultiBonusDuration=8000;
		ultiRange=50;
		ultiAoE=null;
		
		HP=HPMax;
		attLastCast=System.currentTimeMillis();
		passiveLastActivation=System.currentTimeMillis();
		passiveLastCast=System.currentTimeMillis();
		spellLastActivation=System.currentTimeMillis();
		spellLastCast=System.currentTimeMillis();
		ultiLastCast=System.currentTimeMillis()-ultiBonusDuration-1;
	}
	
	public void update() {
		if(spellActive) {
			newAttSpdBonus(Math.min(50,(short)(100-HP*100/getHPMax())));
			if(500+spellLastActivation<System.currentTimeMillis()) {
				spellLastActivation=System.currentTimeMillis();
				setHP(HP-HP*spellHPCost);
			}
		}
		
		if(KKey && getAttTimer()+attLastCast<System.currentTimeMillis() && status.equals("")) {
			target=nearestEnemyUnit();
			if(target!=null)
				if(distanceToUnit(target)<getRange()) {
					attLastCast=System.currentTimeMillis();
					status="attack";
					frameIndex=0;
				}
				else target=null;
		}
		
		if(JKey && spellTimer+spellLastCast<System.currentTimeMillis() && status.equals("")) {
			spellLastCast=System.currentTimeMillis();
			spellActive=!spellActive;
		}
		
		if(LKey && ultiTimer+ultiLastCast<System.currentTimeMillis() && status.equals("")) {
			ultiLastCast=System.currentTimeMillis();
		}
		
		switch(status) {
		case "passive":
			if(frameIndex==0) {
				passiveAoE = new AoE(getX(),getY(),passiveRange,this);
				game.addProjectile(passiveAoE);
			}
			hSpeed=0;
			vSpeed=0;
			if(frameIndex>25 && frameIndex<28) {
				if(passiveDuration+passiveLastCast>System.currentTimeMillis()) {
					if(500+passiveLastActivation<System.currentTimeMillis()) {
						applyLifeSteal(damagingAoE(getX(),getY(),passiveRange,getArmor()*passiveDamage,false,false));
						passiveLastActivation=System.currentTimeMillis();
					}
				}
				else frameIndex=28;
			}
			else
				if(frameIndex<50) frameIndex+=1.2;
				else {
					status="";
					passiveAoE.destroy();
					passiveAoE=null;
				}
			
			break;
		case "attack":
			if(target!=null) {
				if(frameIndex==0 && ultiBonusDuration+ultiLastCast>System.currentTimeMillis()) {
					ultiAoE = new AoE(getX(),getY(),ultiRange,this);
					game.addProjectile(ultiAoE);
				}
				hSpeed=0;
				vSpeed=0;
				direction=getPointDirection45(target.getX(),target.getY());
				switch(direction) {
				case 0:
					spriteIndex=60;
					break;
				case 45:
					spriteIndex=72;
					break;
				case 90:
					spriteIndex=72;
					break;
				case 135:
					spriteIndex=66;
					break;
				case 180:
					spriteIndex=54;
					break;
				case 225:
					spriteIndex=54;
					break;
				case 270:
					spriteIndex=54;
					break;
				case 315:
					spriteIndex=60;
					break;
				}
				
				if(frameIndex>35 && frameIndex<38) {
					if(ultiBonusDuration+ultiLastCast>System.currentTimeMillis()) {
						applyLifeSteal(damagingAoE(getX(),getY(),ultiRange,getDamage(),false,false));
					}
					else damaging(target,getDamage(),true,false);
					
					frameIndex=38;
				}
				
				if(frameIndex<50) frameIndex+=1500/(double)getAttTimer();
				else {
					status="";
					if(ultiAoE!=null){
						ultiAoE.destroy();
						ultiAoE=null;
					}
 				}
			}
			break;
		}
		updateCD();
		super.update();
	}
	
	public boolean isDeath() {
		if(super.isDeath()) {
			spellActive=false;
			if(passiveTimer+passiveLastCast<System.currentTimeMillis() || status.equals("passive")) {
				if(passiveTimer+passiveLastCast<System.currentTimeMillis())  {
					status="passive";
					spriteIndex=78;
					frameIndex=0;
					passiveLastCast=System.currentTimeMillis();
				}
				return false;
			} else return true;
		} else return false;
	}
	
	public boolean isAlive() {
		return !status.equals("passive") && super.isAlive();
	}
	
	
	public void updateCD() {
		if(status.equals("passive")) {
			passiveCD=(short)Math.min(100+100*(passiveLastCast-System.currentTimeMillis())/passiveDuration, 100);
			if(super.isDeath()) passiveColor=false;
			else passiveColor=true;
		}
		else passiveCD=(short)Math.min(100*(System.currentTimeMillis()-passiveLastCast)/passiveTimer, 100);
		if(passiveCD==100) passiveColor=true;
		else passiveColor=false;
		spellCD=(short)Math.min(100*(System.currentTimeMillis()-spellLastCast)/spellTimer, 100);
		spellColor=spellActive;
		if(ultiBonusDuration+ultiLastCast>System.currentTimeMillis()) {
			ultiCD=(short)Math.min(100+100*(ultiLastCast-System.currentTimeMillis())/ultiBonusDuration, 100);
			ultiColor=true;
		} else {
			ultiCD=(short)Math.min(100*(System.currentTimeMillis()-ultiLastCast)/ultiTimer, 100);
			if(ultiCD==100) ultiColor=true;
			else ultiColor=false;
		}
	}
}
