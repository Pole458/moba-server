package Champions;

import game.Game;
import game.Unit;
import projectiles.TargetProjectile;

public class Nightdart extends Champion {
	private short poisonDarts;
	private double passiveDamage;
	private Unit passiveTarget;
	
	private short spellTimer;
	private short spellBonus;
	private boolean spellDart;
	private long spellLastCast;
	
	private boolean ultiBonusAA;
	private short heavyDarts;
	
	public Nightdart(Game game, String name,boolean team) {
		
		super(game, name, team);
		
		sprite = 1;
		width = 10;
		
		downDistance = 40;
		upDistance = 5;
		
		HPMax=200;
		armor=15;
		damage=30;
		attTimer=1250;
		lifesteal=40;
		speed=0.8;
		range=90;
		
		passiveTarget=null;
		poisonDarts=1;
		passiveDamage=0.02;
		
		spellTimer=4000;
		spellBonus=40;
		spellDart=true;
		
		ultiBonusAA=false;
		heavyDarts=0;
		
		HP=HPMax;
		attLastCast=System.currentTimeMillis();
		spellLastCast=System.currentTimeMillis();
	}

	public void update() {
		
		if(passiveTarget!=null)
			if(distanceToUnit(passiveTarget)>range)
				poisonDarts=1;
		
		if(spellDart) {
			newAttSpdBonus(spellBonus);
		}
		else {
			newLifeStealBonus(spellBonus);
		}
		
		if(KKey && getAttTimer()+attLastCast<System.currentTimeMillis() && status.equals("")) {
			target=nearestEnemyUnit();
			if(target!=null)
				if(distanceToUnit(target)<getRange()) {
					attLastCast=System.currentTimeMillis();
					status="attack";
					frameIndex=0;
					if(LKey && heavyDarts==3) {
						ultiBonusAA=true;
						heavyDarts=0;
					}
				}
				else target=null;
		}
		
		if(JKey && spellTimer+spellLastCast<System.currentTimeMillis() && status.equals("")) {
			spellLastCast=System.currentTimeMillis();
			status="spell";
		}
		
		
		switch(status) {
		case "attack":
			if(target!=null) {
				hSpeed=0;
				vSpeed=0;
				direction=getPointDirection45(target.getX(),target.getY());
				switch(direction) {
				case 0:
					spriteIndex=60;
					break;
				case 45:
					spriteIndex=72;
					break;
				case 90:
					spriteIndex=72;
					break;
				case 135:
					spriteIndex=66;
					break;
				case 180:
					spriteIndex=54;
					break;
				case 225:
					spriteIndex=54;
					break;
				case 270:
					spriteIndex=54;
					break;
				case 315:
					spriteIndex=60;
					break;
				}
				
				if(frameIndex>35 && frameIndex<38) {
					game.addProjectile(new TargetProjectile(x,y,6,target,(short)2,(short)16,(short)16,getDamage(),this,true,false));
					frameIndex=38;
				}
				
				if(frameIndex>38 && ultiBonusAA) applyUlti();
				
				if(frameIndex<50) frameIndex+=1500/(double)getAttTimer();
				else {
					status="";
					if(ultiBonusAA)	ultiBonusAA=false;
				}
			}
			break;
		case "spell":
			vSpeed=0;
			hSpeed=0;
			if(getAttTimer()/2+spellLastCast>System.currentTimeMillis()) {
				frameIndex=0;
				switch(direction) {
				case 0:
					spriteIndex=60;
					break;
				case 45:
					spriteIndex=72;
					break;
				case 90:
					spriteIndex=72;
					break;
				case 135:
					spriteIndex=66;
					break;
				case 180:
					spriteIndex=54;
					break;
				case 225:
					spriteIndex=54;
					break;
				case 270:
					spriteIndex=54;
					break;
				case 315:
					spriteIndex=60;
					break;
				}
			}
			else {
				status="";
				spellDart=!spellDart;
				poisonDarts=1;
			}
			break;
		}
		updateCD();
		super.update();
	}
	
	
	public void applyUlti() {
		direction=(short)((direction+180)%360);	
		vSpeed=-1.5*Math.sin(direction*Math.PI/180);
		hSpeed=1.5*Math.cos(direction*Math.PI/180);
	}
	
	public void onHit(Unit target) {
		heavyDarts=(short)Math.min(3, heavyDarts+1);
		
		if(passiveTarget==null) {
			passiveTarget=target;
			poisonDarts=1;
		}
		if(target.equals(passiveTarget)) {
			damaging(target,poisonDarts*getHPMax()*passiveDamage,false,false);
			poisonDarts=(short)Math.min(5, poisonDarts+1);
		} else {
			passiveTarget=target;
			poisonDarts=1;
		}
    	
    	super.onHit(target);
    }
	
	public void updateCD() {
		passiveCD=(short)(100*(poisonDarts)/5);
		if(passiveCD==100) passiveColor=true;
		else passiveColor=false;
		spellCD=(short)Math.min(100*(System.currentTimeMillis()-spellLastCast)/spellTimer, 100);
		if(spellCD==100) spellColor=true;
		else spellColor=false;
		ultiCD=(short)(100*(heavyDarts)/3);
		if(ultiCD==100) ultiColor=true;
		else ultiColor=false;
	}
	
}
