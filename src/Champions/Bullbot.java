package Champions;

import game.Game;
import game.Unit;
import projectiles.AoE;

public class Bullbot extends Champion {
	
	private short passiveTimer;
	private long passiveLastCast;
	private double passiveDamage;
	private short passiveStun;
	
	private short spellTimer;
	private long spellLastCast;
	private short spellRange;
	private double spellDamage;
	private AoE spellAoE;
	
	private int ultiTimer;
	private long ultiLastCast;
	private short ultiDuration;
	private short ultiBonusStats;
	
	public Bullbot(Game game, String name, boolean team) {
		
		super(game, name, team);
		
		sprite = 3;
		width = 10;
		
		downDistance = 75;
		upDistance = 30;
		
		HPMax=340;
		armor=45;
		damage=30;
		attTimer=1660;
		lifesteal=15;
		speed=0.7;
		range=35;
		
		passiveTimer=8000;
		passiveDamage=0.1;
		passiveStun=2000;
		
		spellTimer=8000;
		spellRange=40;
		spellDamage=2;
		spellAoE=null;
		
		ultiTimer=50000;
		ultiDuration=10000;
		ultiBonusStats=50;
				
		HP=HPMax;
		attLastCast=System.currentTimeMillis();
		passiveLastCast=System.currentTimeMillis();
		spellLastCast=System.currentTimeMillis();
		ultiLastCast=System.currentTimeMillis()-ultiDuration-1;
	}
	
	public void update() {
		
		if(KKey && getAttTimer()+attLastCast<System.currentTimeMillis() && status.equals("")) {
			target=nearestEnemyUnit();
			if(target!=null)
				if(distanceToUnit(target)<getRange()) {
					attLastCast=System.currentTimeMillis();
					status="attack";
					frameIndex=0;
				}
				else target=null;
		}
		
		if(JKey && spellTimer+spellLastCast<System.currentTimeMillis() && status.equals("")) {
			spellLastCast=System.currentTimeMillis();
			status="spell";
			frameIndex=0;
		}
		
		if(LKey && ultiTimer+ultiLastCast<System.currentTimeMillis() && status.equals("")) {
			ultiLastCast=System.currentTimeMillis();
			HP+=ultiBonusStats*getHPMax()/100;
			HPMaxBonus+=ultiBonusStats;
		}
		
		if(ultiDuration+ultiLastCast>System.currentTimeMillis()) {
			newHPMaxBonus(ultiBonusStats);
			newArmorBonus(ultiBonusStats);
			newDamageBonus(ultiBonusStats);
			upDistance = 15;
		}
		else upDistance = 30;
		
		switch(status) {
		case "attack":
			if(target!=null) {
				hSpeed=0;
				vSpeed=0;
				direction=getPointDirection45(target.getX(),target.getY());
				switch(direction) {
				case 0:
					spriteIndex=60;
					break;
				case 45:
					spriteIndex=72;
					break;
				case 90:
					spriteIndex=72;
					break;
				case 135:
					spriteIndex=66;
					break;
				case 180:
					spriteIndex=54;
					break;
				case 225:
					spriteIndex=54;
					break;
				case 270:
					spriteIndex=54;
					break;
				case 315:
					spriteIndex=60;
					break;
				}
				
				if(frameIndex>35 && frameIndex<38) {
					damaging(target,getDamage(),true,false);
					frameIndex=38;
				}
				
				if(frameIndex<50) frameIndex+=1500/(double)getAttTimer();
				else {
					status="";
				}
			}
			break;
		case "spell":
			if(frameIndex==0) {
				spellAoE = new AoE(getX(),getY(),spellRange,this);
				game.addProjectile(spellAoE);
			}
			hSpeed=0;
			vSpeed=0;
			spriteIndex=78;
			
			if(frameIndex>35 && frameIndex<38) {
				damagingAoE(getX(),getY(),spellRange,getArmor()*spellDamage,false,true);
				Unit[] enemyHit = getEnemyAoE(getX(),getY(),spellRange);
				for(int i=0; i<enemyHit.length;i++) {
					enemyHit[i].setStatus("knockBack",(short)250);
					enemyHit[i].setDirection(getPointDirection(enemyHit[i].getX(),enemyHit[i].getY()));
				}
				frameIndex=38;
			}
			
			if(frameIndex<50) frameIndex+=1.2;
			else {
				status="";
				spellAoE.destroy();
				spellAoE = null;
			}
			break;
		}
		
		updateCD();
		super.update();
	}
	
	public void onHit(Unit target) {
		if(passiveTimer+passiveLastCast<System.currentTimeMillis()) {
			passiveLastCast=System.currentTimeMillis();
			damaging(target,getHPMax()*passiveDamage,false,false);
			target.setStatus("stun",passiveStun);
		}
		super.onHit(target);
	}
	
	public void framing() {
    	super.framing();
    	if(ultiDuration+ultiLastCast>System.currentTimeMillis()) spriteIndex+=84;
    }
	
	public void updateCD() {
		passiveCD=(short)Math.min(100*(System.currentTimeMillis()-passiveLastCast)/passiveTimer, 100);
		if(passiveCD==100) passiveColor=true;
		else passiveColor=false;
		spellCD=(short)Math.min(100*(System.currentTimeMillis()-spellLastCast)/spellTimer, 100);
		if(spellCD==100) spellColor=true;
		else spellColor=false;
		if(ultiDuration+ultiLastCast>System.currentTimeMillis()) {
			ultiCD=(short)Math.min(100+100*(ultiLastCast-System.currentTimeMillis())/ultiDuration, 100);
			ultiColor=true;
		} else {
			ultiCD=(short)Math.min(100*(System.currentTimeMillis()-ultiLastCast)/ultiTimer, 100);
			if(ultiCD==100) ultiColor=true;
			else ultiColor=false;
		}
	}
}
