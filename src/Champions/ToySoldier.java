package Champions;

import projectiles.AoE;
import game.Game;
import game.Unit;

public class ToySoldier extends Unit {
	private Mastermind creator;
	private short distance;
	
	private AoE attackAoE;

	public ToySoldier(Game game) {
		
		super(game);
		
		name="Pet";
		hSpeed=0;
		vSpeed=0;
		sprite=13;
		frameIndex=0;
		spriteIndex=78;
	
		downDistance = 40;
		upDistance = 5;
		width=10;
		target=null;
		damager=null;
		
		armor=0;
		damage=30;
		lifesteal=0;
		attTimer=1;
		speed=0;
		range=40;
		
		resetBonus();
		
		status="dash";
		distance=100;
		
		attLastCast=System.currentTimeMillis();
	}
	
	public ToySoldier(Game game, double x, double y, short direction, Mastermind creator, short HPMax, short damage) {
		this(game);
		this.x=x;
		this.y=y;
		this.creator=creator;
		this.team=this.creator.getTeam();
		this.HPMax=HPMax;
		this.direction=direction;
		HP=HPMax;
		this.damage=damage;
	}

	public void update() {
		
		if(creator.getKKey() && creator.getLKey() && creator.getStatus().equals("") && creator.getAttTimer()+attLastCast<System.currentTimeMillis() && status.equals("")) {
			target=nearestEnemyUnit();
			if(target!=null)
				if(distanceToUnit(target)<getRange()) {
					attLastCast=System.currentTimeMillis();
					status="attack";
					frameIndex=0;
					creator.soldierAA(this);
				}
				else target=null;
		}
		
		if(creator.orderToDash() && status.equals("")) {
			direction=creator.getDirection();
			status="dash";
			frameIndex=0;
		}
		
		switch(status) {
		case "":
			if(distanceToUnit(creator)>=distance || creator.isDeath() || creator.getStatus().equals("death")) HP=0;
			
			if(super.isDeath()) {
				status="death";
	    		spriteIndex=48;
	    		creator.removeToySoldier();
	    		frameIndex=0;
	    	}
			
			vSpeed=0;
			hSpeed=0;
			
			break;
		case "attack":
			if(frameIndex==0) {
				attackAoE = new AoE(getX(),getY(),getRange(),this);
				game.addProjectile(attackAoE);
			}
			hSpeed=0;
			vSpeed=0;
			direction=getPointDirection45(target.getX(),target.getY());
			switch(direction) {
			case 0:
				spriteIndex=30;
				break;
			case 45:
				spriteIndex=42;
				break;
			case 90:
				spriteIndex=42;
				break;
			case 135:
				spriteIndex=36;
				break;
			case 180:
				spriteIndex=24;
				break;
			case 225:
				spriteIndex=24;
				break;
			case 270:
				spriteIndex=24;
				break;
			case 315:
				spriteIndex=30;
				break;
			}
			
			if(frameIndex>35 && frameIndex<38) {
				damagingAoE(getX(),getY(),getRange(),getDamage(),true,false);
				frameIndex=38;
			}
			
			if(frameIndex<50) frameIndex+=1500/(double)creator.getAttTimer();
			else {
				status="";
				attackAoE.destroy();
				attackAoE=null;
			}
			break;
    	case "death":
    		HP=0.5;
    		hSpeed=0;
    		vSpeed=0;
    		if(frameIndex<50) frameIndex+=1.2;
    		break;
    	case "dash":
    		switch(getDirection45(direction)) {
			case 0:
				spriteIndex=30;
				break;
			case 45:
				spriteIndex=42;
				break;
			case 90:
				spriteIndex=42;
				break;
			case 135:
				spriteIndex=36;
				break;
			case 180:
				spriteIndex=24;
				break;
			case 225:
				spriteIndex=24;
				break;
			case 270:
				spriteIndex=24;
				break;
			case 315:
				spriteIndex=30;
				break;
			}
    		vSpeed=-2*Math.sin(direction*Math.PI/180);
			hSpeed=2*Math.cos(direction*Math.PI/180);
    		
			if(frameIndex<30) frameIndex+=1.2;
			else {
				status="";
				frameIndex=0;
			}
			break;
    	}
		
		framing();
    	wallContact();
    	
    	x+=hSpeed;
    	y+=vSpeed;
		super.update();
	}
	
	public void onHit(Unit target) {
		creator.onHit(target);
	}
	
	public void setStatus(String s, short timer) {
    }
    
    public boolean isDeath() {
    	if(status.equals("death") && frameIndex>=50) return true;
    	else return false;
    }
	
    public void wallContact() {
    	
    	switch(getDirection45(direction)) {
    	case 0:
            if(!game.map.getCell((int)(x+width)/24,(int)(y-width/2)/24)) {
                hSpeed=0;
            }        
    	break;
    	case 45:
    		if(!game.map.getCell((int)(x+width)/24,(int)(y-width)/24)) {
    			hSpeed=0;
    			vSpeed=0;
    		}        
		break;
    	case 90:
    		if(!game.map.getCell((int)(x/24),(int)(y-width)/24)) {
    			vSpeed=0;
    		}        
    	break;
    	case 135:
    		if(!game.map.getCell((int)(x-width)/24,(int)(y-width)/24)) {
    			hSpeed=0;
    			vSpeed=0;
    		}        
    	break;
    	case 180:
    		if(!game.map.getCell((int)(x-width)/24,(int)(y/24))) {
    			hSpeed=0;
    		}        
    	break;
    	case 225:
    		if(!game.map.getCell((int)(x-width)/24,(int)(y+width)/24)) {
    			hSpeed=0;
    			vSpeed=0;
    		}        
    	break;
    	case 270:
    		if(!game.map.getCell((int)(x/24),(int)(y+width)/24)) {
    			vSpeed=0;
    		}        
    	break;
    	case 315:
    		if(!game.map.getCell((int)(x+width)/24,(int)(y+width)/24)) {
    			hSpeed=0;
    			vSpeed=0;
    		}        
		break;
    	}
    }
    
    public void framing() {
    	if(status.equals("")) {
    		if(frameIndex<50) frameIndex+=1.2;
        	else frameIndex=0;
        	spriteIndex=0;
        	switch(direction) {
        	case 0:
        		spriteIndex=6;
        		break;
        	case 45:
        		spriteIndex=18;
        		break;
        	case 90:
        		spriteIndex=18;
        		break;
        	case 135:
        		spriteIndex=12;
        		break;
        	case 180:
        		spriteIndex=0;
        		break;
        	case 225:
        		spriteIndex=0;
        		break;
        	case 270:
        		spriteIndex=0;
        		break;
        	case 315:
        		spriteIndex=6;
        		break;
        	}
    	}
    }
}