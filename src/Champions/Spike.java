package Champions;

import game.Game;
import game.Unit;

import java.awt.Point;

public class Spike extends Champion {
	private double passiveDamage;
	private short spear;
	private long passiveLastCast;
	private short passiveBonusDuration;
	private short passiveBonusAttSpd;
	
	private short spellTimer;
	private long spellLastCast;
	private double spellDamage;
	private Unit[] spellTargets;
	private short spellBonusDuration;
	private long spellLastActivation;
	private short spellRange;
	private Unit spellTarget;
	
	private int ultiTimer;
	private long ultiLastCast;
	private short ultiRange;
	private Unit ultiTarget;
	private short ultiStun;
	
	public Spike(Game game, String name, boolean team) {
		
		super(game, name, team);
		
		sprite = 10;
		width = 10;
		downDistance = 45;
		upDistance = 10;
		
		HPMax=300;
		armor=20;
		damage=30;
		attTimer=1430;
		lifesteal=15;
		speed=0.85;
		range=40;
				
		passiveDamage=0.02;
		spear=0;
		passiveBonusDuration=3000;
		passiveBonusAttSpd=10;
		
		spellTimer=10000;
		spellDamage=0.08;
		spellTargets=null;
		spellTarget=null;
		spellBonusDuration=3000;
		spellRange=60;
		
		ultiTimer=55000;
		ultiRange=60;
		ultiTarget=null;
		ultiStun=2000;
		
		HP=HPMax;
		attLastCast=System.currentTimeMillis();
		passiveLastCast=System.currentTimeMillis();
		spellLastCast=System.currentTimeMillis();
		spellLastActivation=System.currentTimeMillis();
		ultiLastCast=System.currentTimeMillis();
	}
	
	public void update() {
		
		if(passiveBonusDuration+passiveLastCast>System.currentTimeMillis()) {
			newAttSpdBonus(spear*passiveBonusAttSpd);
			//attSpdBonus+=spear*passiveBonusAttSpd;
		} else spear=0;
		
		if(spellBonusDuration+spellLastActivation<System.currentTimeMillis()) spellTargets=null;
		
		if(KKey && getAttTimer()+attLastCast<System.currentTimeMillis() && status.equals("")) {
			target=nearestEnemyUnit();
			if(target!=null)
				if(Point.distance(x,y,target.getX(),target.getY())<getRange()) {
					attLastCast=System.currentTimeMillis();
					status="attack";
					frameIndex=0;
				}
				else target=null;
		}
	
		if(JKey && spellTimer+spellLastCast<System.currentTimeMillis() && status.equals("")) {
			spellTarget=nearestEnemyUnitExcept(spellTargets);
			if(spellTarget!=null)
				if(Point.distance(x,y,spellTarget.getX(),spellTarget.getY())<spellRange) {
					spellLastCast=System.currentTimeMillis();
					status="spell";
					frameIndex=0;
				}
				else spellTarget=null;
		}
		
		if(JKey && spellBonusDuration+spellLastActivation>System.currentTimeMillis() && status.equals("")) {
			spellTarget=nearestEnemyUnitExcept(spellTargets);
			if(spellTarget!=null)
				if(Point.distance(x,y,spellTarget.getX(),spellTarget.getY())<spellRange) {
					spellLastCast=System.currentTimeMillis();
					status="spell";
					frameIndex=0;
				}
				else spellTarget=null;
		}
		
		
		if(LKey && ultiTimer+ultiLastCast<System.currentTimeMillis() && status.equals("")) {
			ultiTarget=nearestEnemyUnit();
			if(ultiTarget!=null)
				if(Point.distance(x,y,ultiTarget.getX(),ultiTarget.getY())<ultiRange) {
					ultiLastCast=System.currentTimeMillis();
					status="ulti";
					frameIndex=0;
					spriteIndex=78;
				}
				else ultiTarget=null;
		}
		
		switch(status) {
		case "attack":
			if(target!=null) {
				hSpeed=0;
				vSpeed=0;
				direction=getPointDirection45(target.getX(),target.getY());
				switch(direction) {
				case 0:
					spriteIndex=60;
					break;
				case 45:
					spriteIndex=72;
					break;
				case 90:
					spriteIndex=72;
					break;
				case 135:
					spriteIndex=66;
					break;
				case 180:
					spriteIndex=54;
					break;
				case 225:
					spriteIndex=54;
					break;
				case 270:
					spriteIndex=54;
					break;
				case 315:
					spriteIndex=60;
					break;
				}
				
				if(frameIndex>35 && frameIndex<38) {
					damaging(target,getDamage(),true,false);
					frameIndex=38;
				}
				
				if(frameIndex<50) frameIndex+=1500/(double)getAttTimer();
				else {
					status="";
				}
			}
			break;
		case "spell":
			if(spellTarget!=null) {
				if(frameIndex<20) {
					hSpeed=0;
					vSpeed=0;
					direction=getPointDirection(spellTarget.getX(), spellTarget.getY());
					switch(getDirection45(direction)) {
					case 0:
						spriteIndex=60;
						break;
					case 45:
						spriteIndex=72;
						break;
					case 90:
						spriteIndex=72;
						break;
					case 135:
						spriteIndex=66;
						break;
					case 180:
						spriteIndex=54;
						break;
					case 225:
						spriteIndex=54;
						break;
					case 270:
						spriteIndex=54;
						break;
					case 315:
						spriteIndex=60;
						break;
					}
				} else {
					vSpeed=-2*Math.sin(direction*Math.PI/180);
					hSpeed=2*Math.cos(direction*Math.PI/180);
				}
				
				if(frameIndex>35 && frameIndex<38) {
					damaging(spellTarget,getHPMax()*spellDamage,true,true);
					addSpellTarget(spellTarget);
					spellLastActivation=System.currentTimeMillis();
					frameIndex=38;
				}
				
				if(frameIndex<50) frameIndex+=1.2;
				else {
					status="";
				}
			}
			break;
		case "ulti":
			if(ultiTarget!=null) {
				hSpeed=0;
				vSpeed=0;
				if(frameIndex==0) {
					x=ultiTarget.getX()-20;
					y=ultiTarget.getY()-20;
					ultiTarget.setStatus("stun", ultiStun);
				}
				
				if((int)frameIndex==20) {
					damaging(ultiTarget,getDamage(),true,false);
				}
				
				if((int)(frameIndex/10)==3) {
					x=ultiTarget.getX()+20;
					y=ultiTarget.getY()-20;
				}
				
				if((int)frameIndex==50) {
					damaging(ultiTarget,getDamage(),true,false);
				}
				
				if((int)(frameIndex/10)==6) {
					x=ultiTarget.getX()-20;
					y=ultiTarget.getY()+20;
				}
				
				if((int)frameIndex==80) {
					damaging(ultiTarget,getDamage(),true,false);
				}
				
				if((int)(frameIndex/10)==9) {
					x=ultiTarget.getX()+20;
					y=ultiTarget.getY()+20;
				}
				
				if((int)frameIndex==110) {
					damaging(ultiTarget,getDamage(),true,false);
				}
				
				if(frameIndex<110) frameIndex+=1.2;
				else {
					x=ultiTarget.getX();
					y=ultiTarget.getY();
					status="";
				}
			}
			break;
		}
		updateCD();
		super.update();
	}
	
	
	private void addSpellTarget(Unit spellTarget) {
		if(spellTargets==null) {
			spellTargets = new Unit[1];
			spellTargets[0] = spellTarget;
		}
		else {
			Unit[] targets = spellTargets;
			spellTargets = new Unit[spellTargets.length+1];
			for(short i=0; i<targets.length;i++)
				spellTargets[i]=targets[i];
			spellTargets[spellTargets.length-1] = spellTarget;
		}
	}
	
	public void onHit(Unit target) {
		damaging(target,getHPMax()*passiveDamage*spear,false,false);
		spear=(short)Math.min(spear+1,5);
		passiveLastCast=System.currentTimeMillis();

		super.onHit(target);
	}
	
	public void updateCD() {
		passiveCD=(short)(20*spear);
		passiveColor=true;
		if(spellBonusDuration+spellLastActivation>System.currentTimeMillis()) {
			spellCD=(short)Math.min(100-100*(System.currentTimeMillis()-spellLastActivation)/spellBonusDuration, 100);
			spellColor=true;
		}
		else {
			spellCD=(short)Math.min(100*(System.currentTimeMillis()-spellLastCast)/spellTimer, 100);
			if(spellCD==100) spellColor=true;
			else spellColor=false;
		}
		ultiCD=(short)Math.min(100*(System.currentTimeMillis()-ultiLastCast)/ultiTimer, 100);
		if(ultiCD==100) ultiColor=true;
		else ultiColor=false;
	}
}