package Champions;

import game.Game;
import game.Unit;
import projectiles.TargetProjectile;

public class Mastermind extends Champion {
	
	private short passiveAttSpd;
	
	private short spellTimer;
	private long spellLastCast;
	private double spellHP;
	private double spellDamage;
	private short spellToySoldier;
	
	private short ultiTimer;
	private long ultiLastCast;
	
	public Mastermind(Game game, String name, boolean team) {
		
		super(game, name,team);
		
		sprite = 11;
		width = 10;
		downDistance = 49;
		upDistance = 5;
		
		HPMax=320;
		armor=12;
		damage=30;
		attTimer=1250;
		lifesteal=12;
		speed=0.7;
		range=100;
		
		passiveAttSpd=20;
		
		spellTimer=8000;
		spellHP=0.5;
		spellDamage=0.15;
		spellToySoldier=0;
		
		ultiTimer=15000;
		
		HP=HPMax;
		attLastCast=System.currentTimeMillis();
		spellLastCast=System.currentTimeMillis();
		ultiLastCast=System.currentTimeMillis();
	}
	
	public void update() {
		
		newAttSpdBonus(spellToySoldier*passiveAttSpd);
		
		if(KKey && !LKey && getAttTimer()+attLastCast<System.currentTimeMillis() && status.equals("")) {
			target=nearestEnemyUnit();
			if(target!=null)
				if(distanceToUnit(target)<getRange()) {
					attLastCast=System.currentTimeMillis();
					status="attack";
					frameIndex=0;
				}
				else target=null;
		}
		
		if(JKey && !LKey && spellTimer+spellLastCast<System.currentTimeMillis() && status.equals("") && spellToySoldier<3) {
			spellLastCast=System.currentTimeMillis();
			status="spell";
			frameIndex=0;
		}
		
		if(JKey && LKey &&ultiTimer+ultiLastCast<System.currentTimeMillis() && status.equals("") ) {
			ultiLastCast=System.currentTimeMillis();
			status="ulti";
			frameIndex=0;
		}
		
		switch(status) {
		case "attack":
			if(target!=null) {
				hSpeed=0;
				vSpeed=0;
				direction=getPointDirection45(target.getX(),target.getY());
				switch(direction) {
				case 0:
					spriteIndex=60;
					break;
				case 45:
					spriteIndex=72;
					break;
				case 90:
					spriteIndex=72;
					break;
				case 135:
					spriteIndex=66;
					break;
				case 180:
					spriteIndex=54;
					break;
				case 225:
					spriteIndex=54;
					break;
				case 270:
					spriteIndex=54;
					break;
				case 315:
					spriteIndex=60;
					break;
				}
				
				if(frameIndex>35 && frameIndex<38) {
					game.addProjectile(new TargetProjectile(x,y,5,target,(short)4,(short)16,(short)16,getDamage(),this,true,false));
					frameIndex=38;
				}
				
				if(frameIndex<50) frameIndex+=1500/(double)getAttTimer();
				else {
					status="";
				}
			}
			break;
		case "spell":
			hSpeed=0;
			vSpeed=0;
			switch(getDirection45(direction)) {
			case 0:
				spriteIndex=60;
				break;
			case 45:
				spriteIndex=72;
				break;
			case 90:
				spriteIndex=72;
				break;
			case 135:
				spriteIndex=66;
				break;
			case 180:
				spriteIndex=54;
				break;
			case 225:
				spriteIndex=54;
				break;
			case 270:
				spriteIndex=54;
				break;
			case 315:
				spriteIndex=60;
				break;
			}
			
			if(frameIndex>35 && frameIndex<38) {
				if(spellToySoldier<3) {
					game.addUnit(new ToySoldier(game, getX(),getY(),getDirection45(direction),this,(short)(getHPMax()*spellHP),(short)(getHPMax()*spellDamage)));
					spellToySoldier++;
				}
				frameIndex=38;
			}
			
			if(frameIndex<50) frameIndex+=1500/(double)getAttTimer();
			else {
				status="";
			}
			break;
		case "animation":
			hSpeed=0;
			vSpeed=0;
			switch(getDirection45(direction)) {
			case 0:
				spriteIndex=60;
				break;
			case 45:
				spriteIndex=72;
				break;
			case 90:
				spriteIndex=72;
				break;
			case 135:
				spriteIndex=66;
				break;
			case 180:
				spriteIndex=54;
				break;
			case 225:
				spriteIndex=54;
				break;
			case 270:
				spriteIndex=54;
				break;
			case 315:
				spriteIndex=60;
				break;
			}
			
			if(frameIndex<50) frameIndex+=1500/(double)getAttTimer();
			else {
				status="";
			}
			break;
		case "ulti":
			hSpeed=0;
			vSpeed=0;
			switch(getDirection45(direction)) {
			case 0:
				spriteIndex=60;
				break;
			case 45:
				spriteIndex=72;
				break;
			case 90:
				spriteIndex=72;
				break;
			case 135:
				spriteIndex=66;
				break;
			case 180:
				spriteIndex=54;
				break;
			case 225:
				spriteIndex=54;
				break;
			case 270:
				spriteIndex=54;
				break;
			case 315:
				spriteIndex=60;
				break;
			}
			
			if(frameIndex<50) frameIndex+=1500/(double)getAttTimer();
			else {
				status="";
			}
			break;
		}
		
		updateCD();
		super.update();
	}
	
	public boolean orderToDash() {
		return status.equals("ulti") && frameIndex>35;
	}
	
	public void removeToySoldier() {
		spellToySoldier--;
	}
	
	public void soldierAA(ToySoldier ts) {
		if(status.equals("")) {
			status="animation";
			frameIndex=0;
		}
		direction=getPointDirection45(ts.getX(),ts.getX());
	}
	
	public void onHit(Unit target) {
		spellLastCast-=spellTimer/10;
		super.onHit(target);
	}
	
	public void updateCD() {
		passiveCD=(short)(spellToySoldier*3/100);
		passiveColor=true;
		spellCD=(short)Math.min(100*(System.currentTimeMillis()-spellLastCast)/spellTimer, 100);
		if(spellCD==100 && spellToySoldier<4) spellColor=true;
		else spellColor=false;
		ultiCD=(short)Math.min(100*(System.currentTimeMillis()-ultiLastCast)/ultiTimer, 100);
		if(ultiCD==100) ultiColor=true;
		else ultiColor=false;
	}
}
