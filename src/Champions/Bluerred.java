package Champions;

import game.Game;
import game.Unit;
import projectiles.AoE;

public class Bluerred extends Champion {
	
	private boolean passiveState;
	private double passiveBonusDamage;
	private boolean passiveBonusAA;
	private short passiveStun;

	private short spellTimer;
	private long spellLastCast;
	private short spellBonus;
	private short passiveForm;
	
	private int ultiTimer;
	private long ultiLastCast;
	private short ultiDuration;
	private double ultiDamage;
	private short ultiRange;
	private AoE ultiAoE;
	
	public Bluerred(Game game, String name, boolean team) {
		
		super(game, name, team);
		
		sprite = 6;
		width = 10;
		
		downDistance = 51;
		upDistance = 4;
		
		HPMax=240;
		armor=35;
		damage=35;
		attTimer=1660;
		lifesteal=15;
		speed=1;
		range=35;
				
		passiveState=false;
		passiveBonusDamage=1.5;
		passiveStun=1500;
		passiveBonusAA=false;
		
		spellTimer=4000;
		spellBonus=40;
		
		ultiTimer=60000;
		ultiDamage=3;
		ultiDuration=8000;
		ultiRange=60;
		ultiAoE=null;
		
		HP=HPMax;
		attLastCast=System.currentTimeMillis();
		spellLastCast=System.currentTimeMillis();
		ultiLastCast=System.currentTimeMillis();
	}
	
	public void update() {
		
		if(passiveState) {
			passiveForm=0;
			newArmorBonus(spellBonus);
		}
		else {
			passiveForm=114;
			newDamageBonus(spellBonus);
		}
		
		if(ultiDuration+ultiLastCast<System.currentTimeMillis() && !visible) {
			status="ulti";
			visible=true;
			frameIndex=0;
		}
		
		if(KKey && getAttTimer()+attLastCast<System.currentTimeMillis() && status.equals("")) {
			target=nearestEnemyUnit();
			if(target!=null)
				if(distanceToUnit(target)<getRange()) {
					attLastCast=System.currentTimeMillis();
					status="attack";
					frameIndex=0;
					visible=true;
				}
				else target=null;
		}
		
		if(JKey && spellTimer+spellLastCast<System.currentTimeMillis() && status.equals("") && visible) {
			spellLastCast=System.currentTimeMillis();
			passiveBonusAA=true;
			passiveState=!passiveState;
		}
		
		if(LKey && ultiTimer+ultiLastCast<System.currentTimeMillis() && status.equals("")) {
			ultiLastCast=System.currentTimeMillis();
			status="ultiChannel";
			frameIndex=0;
		}
		if(LKey && !visible && status.equals("")) {
			status="ulti";
			visible=true;
			frameIndex=0;
		}
		
		switch(status) {
		case "attack":
			if(target!=null) {
				hSpeed=0;
				vSpeed=0;
				direction=getPointDirection45(target.getX(),target.getY());
				switch(direction) {
				case 0:
					spriteIndex=60;
					break;
				case 45:
					spriteIndex=72;
					break;
				case 90:
					spriteIndex=72;
					break;
				case 135:
					spriteIndex=66;
					break;
				case 180:
					spriteIndex=54;
					break;
				case 225:
					spriteIndex=54;
					break;
				case 270:
					spriteIndex=54;
					break;
				case 315:
					spriteIndex=60;
					break;
				}
				
				if(frameIndex>35 && frameIndex<38) {
					damaging(target,getDamage(),true,false);
					frameIndex=38;
				}
				
				if(frameIndex<50) frameIndex+=1500/(double)getAttTimer();
				else {
					status="";
				}
			}
			break;
		case "ultiChannel":
			switch(direction) {
			case 0:
				spriteIndex=84;
				break;
			case 45:
				spriteIndex=96;
				break;
			case 90:
				spriteIndex=96;
				break;
			case 135:
				spriteIndex=90;
				break;
			case 180:
				spriteIndex=78;
				break;
			case 225:
				spriteIndex=78;
				break;
			case 270:
				spriteIndex=78;
				break;
			case 315:
				spriteIndex=84;
				break;
			}
			
			if(frameIndex<50) frameIndex+=1.2;
			else {
				visible=false;
				status="";
			}
			break;
		case "ulti":
			if(frameIndex==0) {
				ultiAoE = new AoE(getX(),getY(),ultiRange,this);
				game.addProjectile(ultiAoE);
			}
			hSpeed=0;
			vSpeed=0;
			if(direction>=180 && direction<=270) spriteIndex=102;
			else spriteIndex=108;
			
			if(frameIndex>35 && frameIndex<38) {
				damagingAoE(getX(),getY(),ultiRange,getDamage()*ultiDamage,false,true);
				Unit[] enemyHit = getEnemyAoE(getX(),getY(),ultiRange);
				for(short i=0; i<enemyHit.length; i++) {
					enemyHit[i].setStatus("knockBack",(short)250);
					enemyHit[i].setDirection(getPointDirection(enemyHit[i].getX(),enemyHit[i].getY()));
				
				}
				frameIndex=38;
			}
			if(frameIndex<50) frameIndex++;
			else {
				status="";
				ultiAoE.destroy();
				ultiAoE = null;
			}
			break;
		}
		
		updateCD();
		super.update();
	}
	
	public void onHit(Unit target) {
		if(passiveBonusAA) {
			if(passiveState)target.setStatus("stun",passiveStun);
			else damaging(target,getDamage()*passiveBonusDamage,false,false);
			passiveBonusAA=false;
		}
		super.onHit(target);
	}
	
	public short getFrame() {
		if(!status.equals("death")) return (short)(spriteIndex+passiveForm+(short)(frameIndex)/10);
		else return (short)(spriteIndex+(short)(frameIndex)/10);
	}
	
	public void updateCD() {
		passiveCD=100;
		if(passiveBonusAA) passiveColor=true;
		else passiveColor=false;
		spellCD=(short)Math.min(100*(System.currentTimeMillis()-spellLastCast)/spellTimer, 100);
		if(spellCD==100 && visible) spellColor=true;
		else spellColor=false;
		ultiCD=(short)Math.min(100*(System.currentTimeMillis()-ultiLastCast)/ultiTimer, 100);
		if(ultiCD==100) ultiColor=true;
		else ultiColor=false;
	}
}
