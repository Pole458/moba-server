package Champions;

import game.Game;
import game.Unit;
import projectiles.AoE;

public class Onslaught extends Champion {
	private double vapor;
	private short passiveStun;

	private short spellTimer;
	private long spellLastCast;
	private boolean spellActive;
	private double spellDamage;
	private short spellRange;
	private long spellLastActivation;
	private AoE spellAoE;
	
	private int ultiTimer;
	private long ultiLastCast;
	private short ultiDuration;
	private short ultiBonusStats;
	
	public Onslaught(Game game, String name, boolean team) {
		
		super(game, name, team);
		
		sprite = 9;
		width = 10;
		downDistance = 80;
		upDistance = 30;
		
		HPMax=330;
		armor=20;
		damage=35;
		attTimer=1660;
		lifesteal=40;
		speed=0.9;
		range=35;
				
		vapor=0;
		passiveStun=4000;
		
		spellTimer=5000;
		spellActive=false;
		spellRange=50;
		spellDamage=0.04;
		spellAoE=null;
		
		ultiTimer=60000;
		ultiDuration=8000;
		ultiBonusStats=100;
		
		HP=HPMax;
		attLastCast=System.currentTimeMillis();
		spellLastCast=System.currentTimeMillis();
		spellLastActivation=System.currentTimeMillis();
		ultiLastCast=System.currentTimeMillis()-ultiDuration-1;
	}
	
	public void update() {
		
		if(isDeath()) spellActive=false;
		
		if(spellActive)	{
			if(spellAoE==null) {
				spellAoE = new AoE(getX(),getY(),spellRange,this);
				game.addProjectile(spellAoE);
			}
			else {
				spellAoE.setX(getX());
				spellAoE.setY(getY());
			}
			vapor=Math.min(vapor+0.2, 100);
			
			if(500+spellLastActivation<System.currentTimeMillis()) {
				applyLifeSteal(damagingAoE(getX(),getY(),spellRange,getHPMax()*spellDamage,false,true));
				spellLastActivation=System.currentTimeMillis();
			}
			
			if(vapor==100) {
				super.setStatus("stun",passiveStun);
				spellActive=false;
			}
		}
		else if(spellAoE!=null) {
			spellAoE.destroy();
			spellAoE=null;
		}
		else vapor=Math.max(vapor-0.1, 0);
		
		if(status.equals("stun"))  vapor=Math.max(vapor-0.3, 0);
		
		if(ultiDuration+ultiLastCast>System.currentTimeMillis()) {
			newLifeStealBonus(ultiBonusStats);
			newSpeedBonus((int)vapor);
			upDistance=15;
		}
		else upDistance=30;
		
		if(KKey && getAttTimer()+attLastCast<System.currentTimeMillis() && status.equals("")) {
			target=nearestEnemyUnit();
			if(target!=null)
				if(distanceToUnit(target)<getRange()) {
					attLastCast=System.currentTimeMillis();
					status="attack";
					frameIndex=0;
					visible=true;
				}
				else target=null;
		}
		
		if(JKey && spellTimer+spellLastCast<System.currentTimeMillis() && status.equals("")) {
			spellLastCast=System.currentTimeMillis();
			spellActive=!spellActive;
		}
		
		if(LKey && ultiTimer+ultiLastCast<System.currentTimeMillis() && status.equals("")) {
			ultiLastCast=System.currentTimeMillis();
		}
		
		switch(status) {
		case "attack":
			if(target!=null) {
				hSpeed=0;
				vSpeed=0;
				direction=getPointDirection45(target.getX(),target.getY());
				switch(direction) {
				case 0:
					spriteIndex=108;
					break;
				case 45:
					spriteIndex=120;
					break;
				case 90:
					spriteIndex=120;
					break;
				case 135:
					spriteIndex=114;
					break;
				case 180:
					spriteIndex=102;
					break;
				case 225:
					spriteIndex=102;
					break;
				case 270:
					spriteIndex=102;
					break;
				case 315:
					spriteIndex=108;
					break;
				}
				
				if(frameIndex>35 && frameIndex<38) {
					damaging(target,getDamage(),true,false);
					
					frameIndex=38;
				}
				
				if(frameIndex<50) frameIndex+=1500/(double)getAttTimer();
				else {
					status="";
				}
			}
			break;
		}
		
		updateCD();
		super.update();
	}
	
	public void setStatus(String s, short timer) {
		if(!(ultiDuration+ultiLastCast>System.currentTimeMillis())) super.setStatus(s, timer);
	}
	
	public void onHit(Unit target) {
		if(vapor>75) {
			target.setStatus("knockBack", (short)250);
			target.setDirection(getPointDirection(target.getX(),target.getY()));
			vapor-=50;
		}
		else vapor=Math.max(vapor-10, 0);
		super.onHit(target);
	}
	
	public short getFrame() {
		short spriteBonus=0;
		if(ultiDuration+ultiLastCast>System.currentTimeMillis() && (status.equals("") || status.equals("attack")) ) spriteBonus+=126;
		if(spellActive && !status.equals("death") && !status.equals("attack")) spriteBonus+=54;
    	return (short)(spriteIndex+spriteBonus+(short)(frameIndex)/10);
	}
	
	public void updateCD() {
		passiveCD=(short)vapor;
		if(vapor<75) passiveColor=true;
		else passiveColor=false;
		spellCD=(short)Math.min(100*(System.currentTimeMillis()-spellLastCast)/spellTimer, 100);
		if(spellCD==100) spellColor=true;
		else spellColor=false;
		ultiCD=(short)Math.min(100*(System.currentTimeMillis()-ultiLastCast)/ultiTimer, 100);
		if(ultiCD==100) ultiColor=true;
		else ultiColor=false;
	}
}