package Champions;

import game.Game;
import game.Unit;
import projectiles.AoE;
import projectiles.TargetProjectile;

public class Bullseye extends Champion {
	
	private short spellTimer;
	private long spellLastCast;
	private short spellDuration;
	private short spellArmorReduction;
	private short spellMinRange;
	private AoE spellMinAoE;
	private AoE spellMaxAoE;
	
	private int ultiTimer;
	private long ultiLastCast;
	private Unit ultiSecondTarget;
	private short ultiDuration;
	
	public Bullseye(Game game, String name, boolean team) {
		
		super(game, name,team);
		
		sprite = 12;
		width = 10;
		downDistance = 40;
		upDistance = 5;
		
		HPMax=210;
		armor=10;
		damage=40;
		attTimer=1530;
		lifesteal=50;
		speed=0.7;
		range=120;
		
		spellTimer=15000;
		spellArmorReduction=50;
		spellDuration=6000;
		spellMinRange=100;
		
		
		ultiTimer=55000;
		ultiSecondTarget=null;
		ultiDuration=8000;
		
		HP=HPMax;
		attLastCast=System.currentTimeMillis();
		spellLastCast=System.currentTimeMillis()-spellDuration-1;
		ultiLastCast=System.currentTimeMillis()-ultiDuration-1;
	}
	
	public void update() {
		
		if(spellDuration+spellLastCast>System.currentTimeMillis()) {
			Unit[] spellTargets = getEnemyAoE(getX(), getY(), range);
			if(spellTargets!=null) {
				for(short i=0; i<spellTargets.length; i++)
					if(distanceToUnit(spellTargets[i])>spellMinRange)
						spellTargets[i].newArmorBonus(-spellArmorReduction);
			}
			if(spellMinAoE==null) {
				spellMinAoE = new AoE(getX(),getY(),spellMinRange,this);
				game.addProjectile(spellMinAoE);
			} else {
				spellMinAoE.setX(getX());
				spellMinAoE.setY(getY());
			}
			if(spellMaxAoE==null) {
				spellMaxAoE = new AoE(getX(),getY(),range,this);
				game.addProjectile(spellMaxAoE);
			} else {
				spellMaxAoE.setX(getX());
				spellMaxAoE.setY(getY());
			}
		} else {
			if(spellMinAoE!=null) {
				spellMinAoE.destroy();
				spellMinAoE=null;
			}
			if(spellMaxAoE!=null) {
				spellMaxAoE.destroy();
				spellMaxAoE=null;
			}
		}
		
		if(KKey && getAttTimer()+attLastCast<System.currentTimeMillis() && status.equals("")) {
			target=nearestEnemyUnit();
			if(target!=null)
				if(distanceToUnit(target)<getRange()) {
					attLastCast=System.currentTimeMillis();
					status="attack";
					frameIndex=0;
				}
				else target=null;
		}
		
		if(JKey && spellTimer+spellLastCast<System.currentTimeMillis() && status.equals("")) {
			spellLastCast=System.currentTimeMillis();
		}
		
		if(LKey && ultiTimer+ultiLastCast<System.currentTimeMillis() && status.equals("") ) {
			ultiLastCast=System.currentTimeMillis();
		}
		
		
		switch(status) {
		case "attack":
			if(target!=null) {
				hSpeed=0;
				vSpeed=0;
				direction=getPointDirection45(target.getX(),target.getY());
				switch(direction) {
				case 0:
					spriteIndex=60;
					break;
				case 45:
					spriteIndex=72;
					break;
				case 90:
					spriteIndex=72;
					break;
				case 135:
					spriteIndex=66;
					break;
				case 180:
					spriteIndex=54;
					break;
				case 225:
					spriteIndex=54;
					break;
				case 270:
					spriteIndex=54;
					break;
				case 315:
					spriteIndex=60;
					break;
				}
				
				if(frameIndex>35 && frameIndex<38) {
					game.addProjectile(new TargetProjectile(x,y,6,target,(short)2,(short)16,(short)16,getDamage()*getPassiveDamage(target),this,true,false));
					
					if(ultiDuration+ultiLastCast>System.currentTimeMillis()) {
						ultiSecondTarget=nearestEnemyUnitExcept(target);
						if(ultiSecondTarget!=null) game.addProjectile(new TargetProjectile(x,y,6,ultiSecondTarget,(short)2,(short)16,(short)16,getDamage()*getPassiveDamage(ultiSecondTarget),this,true,false));
						
					}
					frameIndex=38;
				}
				
				if(frameIndex<50) frameIndex+=1500/(double)getAttTimer();
				else {
					status="";
				}
			}
			break;
		}
		updateCD();
		super.update();
	}
	
	public double getPassiveDamage(Unit target) {
		return (60+Math.max(0,(distanceToUnit(target)-80)*2))/100;
	}
	
	public void updateCD() {
		passiveColor=true;
		passiveCD=100;
		if(spellDuration+spellLastCast>System.currentTimeMillis()) {
			spellCD=(short)Math.min(100+100*(spellLastCast-System.currentTimeMillis())/spellDuration, 100);
			spellColor=true;
		} else {
			spellCD=(short)Math.min(100*(System.currentTimeMillis()-spellLastCast)/spellTimer, 100);
			if(spellCD==100) spellColor=true;
			else spellColor=false;
		}
		if(ultiDuration+ultiLastCast>System.currentTimeMillis()) {
			ultiCD=(short)Math.min(100+100*(ultiLastCast-System.currentTimeMillis())/ultiDuration, 100);
			ultiColor=true;
		} else {
			ultiCD=(short)Math.min(100*(System.currentTimeMillis()-ultiLastCast)/ultiTimer, 100);
			if(ultiCD==100) ultiColor=true;
			else ultiColor=false;
		}
	}
	
}