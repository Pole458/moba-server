package projectiles;

import game.Map;

public class Projectile {
	
	protected double x;
	protected double y;
	protected short sprite;
	protected double frameIndex;
	protected short height;
	protected short width;
	protected boolean destroy;
	
	public Projectile() {
		
		frameIndex = 0;
		destroy = false;
	
	}
	
	public Projectile(double x, double y, short projectileNum, short width, short height) {
		
		this();
		this.x = x;
		this.y = y;
		this.sprite=projectileNum;
		this.width=width;
		this.height=height;
	}

	public Projectile(Projectile p) {
		
		this.x = p.x;
		this.y = p.y;
		this.sprite = p.sprite;
		this.frameIndex = p.frameIndex;
		this.height = p.height;
		this.width = p.width;
	}
	
	public boolean update() {
		return !destroy;
	}
	
	public static Bomb copy(Bomb b) {
		return new Bomb(b);
	}
	
	public static Mark copy(Mark m) {
		return new Mark(m);
	}
	
	public static TargetProjectile copy(TargetProjectile tp) {
		return new TargetProjectile(tp);
	}
	
	protected boolean inMap() {
		if(x>0 && x<Map.tileX*Map.cellSize && y>0 && y<Map.tileY*Map.cellSize) return true;
		return false;
	}
	
	//GETTERS & SETTERS////////////////////////////////////////////////////////////////////////////////////////////
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	public short getFrame() {
		return (short)frameIndex;
	}
	public short getSprite() {
		return sprite;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public short getRange() {
		return 0;
	}
	
	public char getTeam() {
		return 'N';
	}
	
	public void setSprite(short s) {
		sprite=s;
	}
	
	public void setFrameIndex(short f) {
		frameIndex=f;
	}
	public void destroy() {
		destroy=true;
	}
	public boolean isDestroyed() {
		return destroy;
	}
}

