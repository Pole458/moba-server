package projectiles;

import java.awt.Rectangle;
import game.Unit;

public class TargetProjectile extends Projectile {
	protected Unit target;
	protected double speed;
	protected double hSpeed;
	protected double vSpeed;
	protected short direction;
	protected boolean onHitProc;
	protected boolean spellHitProc;
	protected double damage;
	protected Unit damager;
	
	public TargetProjectile() {
		super();
	}
	
	public TargetProjectile(double x, double y, double speed, Unit target, short sprite, short width, short height, double damage, Unit damager,boolean onHitProc,boolean spellHitProc) {
		super(x, y, sprite, width, height);
		this.speed=speed;
		this.target=target;
		this.damage=damage;
		this.damager=damager;
		this.onHitProc=onHitProc;
		this.spellHitProc=spellHitProc;
	}
	
	public TargetProjectile(TargetProjectile p) {
		super(p);
		this.target=p.target;
		this.speed=p.speed;
		this.hSpeed=p.hSpeed;
		this.vSpeed=p.vSpeed;
		this.direction=p.direction;
		this.onHitProc=p.onHitProc;
		this.spellHitProc=p.spellHitProc;
		this.damage=p.damage;
		this.damager=p.damager;
	}
	
	public boolean update() {
		if(target!=null) {
		
			direction=getPointDirection(target.getX(),target.getY());
			
			framing();
			
			if(getArea().contains(target.getX(),target.getY())) {
				damager.damaging(target,damage,onHitProc,spellHitProc);
				return false;
			}
			
			vSpeed=-speed*Math.sin(direction*Math.PI/180);
    		hSpeed=speed*Math.cos(direction*Math.PI/180);
    		
    		x+=hSpeed;
    		y+=vSpeed;
    		
			return super.update();
		}
		else return false;
	}

	public void framing() {
		switch(getDirection45(direction)) {
		case 0:
			frameIndex=0;
			break;
		case 45:
			frameIndex=1;
			break;
		case 90:
			frameIndex=2;
			break;
		case 135:
			frameIndex=3;
			break;
		case 180:
			frameIndex=4;
			break;
		case 225:
			frameIndex=5;
			break;
		case 270:
			frameIndex=6;
			break;
		case 315:
			frameIndex=7;
			break;
		}
	}
	
	protected short getPointDirection(double x, double y) {
		short degrees = (short)(Math.atan((y-this.y)/(this.x-x))*180/Math.PI);
		
		if(x<this.x) degrees+=180;
		if(x>=this.x && y>this.y) degrees+=360;
		
		return degrees;
	}
	
	protected int getDirection45(int degrees) {
		if(degrees>337.5 || degrees<22.5) degrees=0;
		if(degrees>22.5 && degrees<67.5) degrees=45;
		if(degrees>67.5 && degrees<112.5) degrees=90;
		if(degrees>112.5 && degrees<157.5) degrees=135;
		if(degrees>157.5 && degrees<202.5) degrees=180;
		if(degrees>202.5 && degrees<247.5) degrees=225;
		if(degrees>247.5 && degrees<292.5) degrees=270;
		if(degrees>292.5 && degrees<337.5) degrees=315;
		
		return degrees;
	}
	
	public Rectangle getArea() {
		return new Rectangle((int)x-width/2,(int)y-height/2,width,height);
	}
	public Unit getDamger() {
		return damager;
	}
	public double getDamage() {
		return damage;
	}
	public Unit getTarget() {
		return target;
	}
	
}
