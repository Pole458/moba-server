package projectiles;

import game.Unit;

public class AoE extends Projectile {
	private short range;
	private Unit creator;
	
	public AoE(double x, double y, short range, Unit creator) {
		super(x,y,(short)0,(short)0,(short)0);
		this.range=range;
		this.creator=creator;
		this.destroy=false;;
	}
	
	public boolean update() {
		if(destroy) return false;
		else return super.update();
	}
	
	public short getRange() {
		return range;
	}
	public char getTeam() {
		 return creator.getTeam();
	}
	
	public void setX(double x) {
		this.x=x;
	}
	
	public void setY(double y) {
		this.y=y;
	}
} 
