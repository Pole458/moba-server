package projectiles;

import Champions.Champion;
import game.Unit;

public class Bomb extends Projectile {
	private double x;
	private double y;
	private Unit target;
	private short spriteIndex;
	private String status;
	
	public Bomb() {
		spriteIndex = 0;
		status = "waiting";
	}
	
	public Bomb(Bomb b) {
		super(b);
		this.x=b.x;
		this.y=b.y;
		this.target=b.target;
		this.frameIndex=b.frameIndex;
		this.spriteIndex=b.spriteIndex;
		this.status=b.status;
	}
	
	public Bomb(double x, double y, Unit t) {
		this();
		this.x=x;
		this.y=y;
		target=t;
	}
	
	public boolean update() {
		
		switch(status) {
		
		case "waiting":
			spriteIndex = 0;
			break;
		
		case "picked":
			if(!target.isAlive()) {
				spriteIndex = 0;
				y = target.getY() - 12;
				status = "waiting";
				target = null;
			}
			else {
				x = target.getX();
				y = target.getY() - target.getDownDistance() + target.getUpDistance() - 10;
				spriteIndex = 4;
			}
			break;
		}
			
		if(frameIndex<25) frameIndex++;
		else frameIndex=0;
		
		return true;
	}
	
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	public double getCollisionY() {
		
		if(status.equals("waiting")) 
			return y + 12;
		else {
			if(target != null) return target.getY();
			else return y;
		}
	}
	
	public short getSprite() {
		return 1;
	}
	
	public short getFrame() {
		return (short)(spriteIndex+frameIndex/10);
	}
	
	public void checkBomb(Champion c) {
		if(target == null && c.getSize().contains(x, getCollisionY()) && c.isAlive() ) {
			target = c;
			status = "picked";
		}
	}
	
	public String getStatus() {
		return status;
	}
}
