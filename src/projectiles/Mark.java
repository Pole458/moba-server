package projectiles;

import game.Unit;

public class Mark extends Projectile {
	private Unit target;
	private Unit damager;
	private long spawnTime;
	private boolean destroy;
	
	public Mark(Unit target, Unit damager, short sprite, double frameIndex, short width, short height) {
		super(target.getX(), target.getY(), sprite, width, height);
		this.target=target;
		this.damager=damager;
		this.frameIndex=frameIndex;
		spawnTime=System.currentTimeMillis();
		destroy=false;
	}
	
	public Mark(Mark m) {
		super(m);
		this.target=m.target;
		this.damager=m.damager;
		this.spawnTime=m.spawnTime;
		this.destroy=m.destroy;
	}
	
	public void destroy() {
		destroy=true;
	}
	
	public void setTarget(Unit target) {
		this.target=target;
	}
	
	public boolean update() {
		
		if(target!=null && !destroy) {
			
			if(frameIndex<25) frameIndex++;
			else frameIndex=0;
			
			x = target.getX();
			y = target.getY() - target.getDownDistance() + target.getUpDistance() - 10;
			
			return super.update();
		}
		else return false;
	}
	
	public Unit getTarget() {
		return target;
	}
	
	public long getSpawnTime() {
		return spawnTime;
	}
	
	public short getFrame() {
		return (short)(frameIndex/10);
	}
	
	public Unit getDamager() {
		return damager;
	}
	
}