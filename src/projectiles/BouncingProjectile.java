package projectiles;

import game.Unit;

public class BouncingProjectile extends TargetProjectile {
	private short bounce;
	private short bounceRange;

	public BouncingProjectile(double x, double y, double speed, Unit target, short sprite, short width, short height, double damage, Unit damager,boolean onHitProc,boolean spellHitProc, short bounce, short bounceRange) {
		super(x,y,speed,target,sprite,width,height,damage,damager,onHitProc,spellHitProc);
		this.bounce=bounce;
		this.bounceRange=bounceRange;
	}
	
	public BouncingProjectile(BouncingProjectile bp) {
		super(bp);
		this.bounce=bp.bounce;
		this.bounceRange=bp.bounceRange;
	}
	
	public boolean update() {
		
		if(target!=null) {
			
			direction=getPointDirection(target.getX(),target.getY());
			
			framing();
			
			if(getArea().contains(target.getX(),target.getY())) {
				damager.damaging(target,damage,onHitProc,spellHitProc);
				if(bounce>0) {
					target = target.nearestAllyUnitExcept(target);
					if(target!=null) {
						if(target.distanceToProjectile(this)<bounceRange) bounce--;
						else destroy=true;
					} else destroy=true;
				}
				else destroy=true;
			}
			
			vSpeed=-speed*Math.sin(direction*Math.PI/180);
    		hSpeed=speed*Math.cos(direction*Math.PI/180);
    		
    		x+=hSpeed;
    		y+=vSpeed;
    	
		}
		else destroy=true;
		
		return !destroy;
	}
	
	public short getBounce() {
		return bounce;
	}
	
}
