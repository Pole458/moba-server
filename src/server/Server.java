package server;

import game.Game;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

@SuppressWarnings("serial")
public class Server extends JFrame implements Runnable {
	
	//DEBUG
	public static boolean debug = true;
	public static JTextArea DebugWindow = new JTextArea();
	public JScrollPane scroll;
	public static JTextArea DebugServerSituation;
	
	public static final short MAXPLAYERS = 16;
	public static final int SERVERPORT = 1900;
	
	private static User[] Users;
	private static short connectedUsers;
	
	public Thread LogInThread;
	
	public static Lobby[] lobby;
	public static ChampionSelect[] championSelect;
	public static Game[] game;
	
	public Server() {
		
		//DEBUG
		
		if(debug) {
			setTitle("Server");
			DebugWindow = new JTextArea();
			DebugWindow.setEditable(false);
			DebugServerSituation = new JTextArea();
			DebugServerSituation.setEditable(false);
			scroll = new JScrollPane(DebugWindow);
			add("Center", scroll);
			add("North",DebugServerSituation);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setSize(400,300);
			setVisible(true);

			Thread animator = new Thread(this, "animator");
			animator.start();
		}
		
		Server.DebugLog("Server started\n");
		
		connectedUsers = 0;
		Users = new User[Server.MAXPLAYERS];
		
		lobby = new Lobby[0];
		championSelect = new ChampionSelect[0];
		game = new Game[0];
		
		LogInThread = new Thread(new LogInThread(), "LogInThread");
		LogInThread.start();
	
	}
	
	@Override
	public void run() {
		
		while(true) {	
			try {
				
				DebugServerSituation.setText("Players: " + connectedUsers);
				if(lobby != null) DebugServerSituation.append(", Lobby: " + lobby.length);
				if(championSelect != null) DebugServerSituation.append(", ChampSelect: " + championSelect.length);
				if(game != null) DebugServerSituation.append(", Game: " + game.length);
				
				repaint();
				
				if(Thread.currentThread().isInterrupted()) throw new InterruptedException();
				
				Thread.sleep(1000/33);
            } catch (InterruptedException e) {
            	break;
            }
		}
	}

	public static void DebugLog(String s) {
		if(debug) Server.DebugWindow.append(s);
	}
	
	public static void writeString(DataOutputStream outDataStream, String s) {
		short l = (short) s.length();
		try {
			outDataStream.writeShort(l);
			for(int i=0; i<l; i++) {
				outDataStream.writeChar(s.charAt(i));
			}
		} catch (IOException e) {
		}
	}
	
	public static String readString(DataInputStream inDataStream) {
		try {
			short l = inDataStream.readShort();
			String s = new String("");
			for(int i=0; i<l; i++) {
				s = s + inDataStream.readChar();
			}
			return s;
		} catch (IOException e) {
			return null;
		}
	}
	
	public static void msgToClient(DataOutputStream outDataStream, String s) throws IOException {
		
		switch(s) {
		case "Connected":
			outDataStream.write(1);
			break;
		case "NameAlreadyUsed":
			outDataStream.write(2);
			break;
		case "ServerIsFull":
			outDataStream.write(3);
			break;
		case "Invite":
			outDataStream.write(4);
			break;
		case "LobbyException":
			outDataStream.write(5);
			break;
		case "CreatedGameData":
			outDataStream.write(6);
			break;
		case "CreatedChampionSelectData":
			outDataStream.write(7);
			break;
		case "LobbyData":
			outDataStream.write(8);
			break;
		case "ExitLobby":
			outDataStream.write(9);
			break;
		case "UpdatedChampionSelectData":
			outDataStream.write(10);
			break;
		case "GameData":
			outDataStream.write(11);
			break;
		/*case "ReconnectedGameData":
			outDataStream.write(12);
			break;*/
			
		default: break;
		}
	}

	public static boolean isOnline(User u) {
		
		for(short i=0; i<connectedUsers; i++) {
			if( Users[i].equals(u) ) return true;
		}
		return false;
	}
	
	public static void LogInUser(User newUser) throws LogInException {
		
		if(connectedUsers >= Server.MAXPLAYERS) throw new LogInException("ServerIsFull");
		
		if( isOnline(newUser) ) throw new LogInException("NameAlreadyUsed");
		
		Users[connectedUsers] = newUser;
		connectedUsers++;
		
		Game g = findGame( newUser.getUserName() );
		
		if( g != null) throw new LogInException("UserReconnected",g);
		
		Server.DebugLog("New user connected: " + newUser.getUserName() + " \n");
		
	}
	
	public static void LogOutUser(User user) {
		
		for(short i=0, j=0; i+j<connectedUsers; i++ ) {
			
			if( Users[i].equals(user) ) {
				for(j=0; i+j<connectedUsers-1; j++) {
					Users[i+j] = Users[i+j+1];
				}
				connectedUsers--;
				Server.DebugLog("User disconnected: " + user.getUserName() + " \n");
			}
		
		}
		//If the user was in a lobby, he get kicked out
		if( user.getStatus() == "Lobby" )
			 findLobby(user).removeUser(user);
		
		//if the user was in champion select, the remaining players are kicked into a lobby
		if( user.getStatus() == "ChampionSelect") {
			createLobby( findChampionSelect(user) );
			destroyChampionSelect( findChampionSelect(user) );
		}
		
		//if the user was in game, set him as disconnected. If all the players disconnect, the game is deleted
		if( user.getStatus() == "Game") {
			Game game = findGame( user );
			game.disconnectPlayer(user);
			if(game.playersConnected() == 0)
				destroyGame(game);
		}
			
	}

	public static User findUser(String name) {
		
		for(short i=0; i<connectedUsers; i++)
			if(Users[i].getUserName().equals(name)) return Users[i];
		
		return null;
	}

	public static void sendInvite(User inviter, String invited) throws LobbyException, IOException {
		
		User userInvited = findUser(invited);
		
		if(userInvited == null) {
			Server.DebugLog("The user invited is not online.\n");
			throw new LobbyException("The user invited is not online");
		}
	
		if(userInvited.getStatus() == "ChampionSelect") {
			Server.DebugLog("The user is now in champion select.\n");
			throw new LobbyException("The user is in Champ Select");
		}
		
		if(userInvited.getStatus() == "Game") {
			Server.DebugLog("The user invited is now in game.\n");
			throw new LobbyException("The user invited is in Game");
		}
		
		ByteArrayOutputStream packet = new ByteArrayOutputStream();
		DataOutputStream stream = new DataOutputStream(packet);
		
		msgToClient(stream,"Invite");	//Tells the user that he was invited...
		
		Server.writeString(stream, inviter.getUserName());		//... by this other user
		
		userInvited.sendToClient(packet);
		
		Server.DebugLog(inviter.getUserName() +  " invited " + invited + " to his lobby.\n");
		
	}

	public static Lobby createLobby(User u) throws LobbyException {
		
		Lobby l = new Lobby();

		Lobby[] lobbies = new Lobby[lobby.length+1];
		
		short i;
		for(i=0; i<lobby.length; i++)
			lobbies[i] = lobby[i];
		
		lobbies[i] = l;
		
		lobby = lobbies;
			
		//}
		
		l.addUser(u);
		
		u.setStatus("Lobby");
		
		Server.DebugLog("New lobby created by: " + u.getUserName() + " \n");
		Server.DebugLog("Current lobbies: " + lobby.length + " \n");
		
		return l;
	}
	
	public static Lobby createLobby(ChampionSelect c) {
		
		Lobby l = new Lobby(c);
		
		if(l.getCurrentPlayers() == 0) return null;		//if the lobby created is empty, stop before create it
	
		Lobby[] lobbies = new Lobby[lobby.length+1];
		
		short i;
		for(i=0; i<lobby.length; i++)
			lobbies[i] = lobby[i];
		
		lobbies[i] = l;
		
		lobby = lobbies;
		
		Server.updateLobby(l);
		
		Server.DebugLog("New lobby created after Champion Select.\n");
		Server.DebugLog("Current lobbies: " + lobby.length + " \n");
		
		return l;
	}
	
	public static Lobby createLobby(Game g) {
		
		Lobby l = new Lobby(g);
		
		if(l.getCurrentPlayers() == 0) return null;		//if the lobby created is empty, stop before create it
	
		Lobby[] lobbies = new Lobby[lobby.length+1];
		
		short i;
		for(i=0; i<lobby.length; i++)
			lobbies[i] = lobby[i];
		
		lobbies[i] = l;
		
		lobby = lobbies;
		
		Server.updateLobby(l);
		
		Server.DebugLog("New lobby created after Game\n");
		Server.DebugLog("Current lobbies: " + lobby.length + " \n");
		
		return l;
	}
	
	public static Lobby joinLobby(User invited, User inviter) throws LobbyException {
		
		if(invited.getStatus() == "Lobby") {		//If the invited is already in a lobby, he needs to exit
			Server.findLobby(invited).removeUser(invited);
			invited.setStatus("MainPage");
		}
		
		Server.DebugLog(invited.getUserName() + " accepted the invite of " + inviter.getUserName() + '\n');
		
		Lobby l = Server.findLobby(inviter);
		
		l.addUser(invited);
		
		invited.setStatus("Lobby");
		
		Server.DebugLog(invited.getUserName() +" joined the lobby created by " + inviter.getUserName() + '\n');
		
		return l;
	}

	public static void sendLobbyData(User u, Lobby lobby) throws IOException {
		
		ByteArrayOutputStream packet = new ByteArrayOutputStream();
		DataOutputStream stream = new DataOutputStream(packet);
		
		msgToClient(stream,"LobbyData");
		
		stream.writeShort(lobby.getCurrentPlayers());
		
		for(short i=0; i<lobby.getCurrentPlayers(); i++) {		//For each user in the lobby...
			
			Server.writeString(stream, lobby.getUser(i).getUserName());		//...sends the user name...
			stream.writeBoolean(lobby.getUserTeam(i));						//...and the team
		
		}
		
		u.sendToClient( packet );
		
	}

	public static void updateLobby(Lobby l) {
		
		for(short i=0; i<l.getCurrentPlayers(); i++) {	//Send the update lobby situation to all user in that lobby
			
			try {
				sendLobbyData(l.getUser(i), l);
			} catch (IOException e) {
				Server.DebugLog("Could not send Lobby data to " + l.getUser(i).getUserName() + '\n');
			}
		
		}
	}

	public static void exitLobby(User user) throws IOException {
		
		Server.findLobby(user).removeUser(user);
		
		ByteArrayOutputStream packet = new ByteArrayOutputStream();
		DataOutputStream stream = new DataOutputStream(packet);
		
		Server.msgToClient(stream,"ExitLobby");
		
		user.sendToClient( packet );
		
		user.setStatus("MainPage");
	}
	
	public static void destroyLobby(Lobby l) {
			
		boolean found = false;
		Lobby[] lobbies = new Lobby[lobby.length-1];
		
		for(short i=0; i<lobbies.length; i++) {
		
			if(lobby[i] == l)
				found = true;
			if(found)
				lobbies[i] = lobby[i+1];
			else
				lobbies[i] = lobby[i];
		}
		
		lobby = lobbies;
		
		Server.DebugLog("Lobby destroyed.\n");
		if(lobby != null) Server.DebugLog("Current lobbies: " + lobby.length + " \n");
		else Server.DebugLog("Current lobbies: 0\n");
	}
	
	public static Lobby findLobby(User u) {
		
		for(short i=0; i<lobby.length; i++) 
			if(lobby[i].isInLobby(u)) return lobby[i];
		
		return null;
	}

	public static ChampionSelect createChampionSelect(User user) {		//Creates a champion select, destroy the lobby and send the informations to all users in champ select
		
		Lobby l = findLobby(user);
		
		ChampionSelect c = new ChampionSelect(l);
			
		ChampionSelect[] champSel = new ChampionSelect[championSelect.length+1];
		
		short i;
		for(i=0; i<championSelect.length; i++) 
			champSel[i] = championSelect[i];
		
		champSel[i] = c;
		
		championSelect = champSel;
		
		destroyLobby(l);
		
		for(i=0; i<c.getPlayers(); i++) {	//Send the update championSelect situation to all user in that championSelect
			
			try {
				sendCreatedChampionSelectData(c.getUser(i), c);
			} catch (IOException e) {
				Server.DebugLog("Could not send ChampionSelect data to " + c.getUser(i).getUserName() + '\n' );
			}
		
		}
		
		Server.DebugLog("New Champion Select created by: " + user.getUserName() + " \n");
		Server.DebugLog("Current ChampionSelect: " + championSelect.length + " \n");
		
		return c;
		
	}
	
	public static void destroyChampionSelect(ChampionSelect c) {
		
		boolean found = false;
		ChampionSelect[] champSel = new ChampionSelect[championSelect.length-1];
		
		
		for(short i=0; i<champSel.length; i++) {
			
			if(championSelect[i] == c)
				found = true;
			if(found)
				champSel[i] = championSelect[i+1];
			else 
				champSel[i] = championSelect[i];
		}
		
		championSelect = champSel;
		
		Server.DebugLog("Champion Select destroyed.\n");
		if(championSelect != null) Server.DebugLog("Current Champion Select: " + championSelect.length + " \n");
		else Server.DebugLog("Current Champion Select: 0\n");
	}
	
	public static ChampionSelect findChampionSelect(User u) {
		
		for(short i=0; i<championSelect.length; i++)
			if(championSelect[i].isInChampionSelect(u) ) return championSelect[i];
		
		return null;
	}

	public static void sendCreatedChampionSelectData(User u, ChampionSelect champSel) throws IOException {
		
		ByteArrayOutputStream packet = new ByteArrayOutputStream();
		DataOutputStream stream = new DataOutputStream(packet);
		
		msgToClient(stream,"CreatedChampionSelectData");
		stream.writeShort(champSel.getPlayers());
		
		for(short i=0; i<champSel.getPlayers(); i++) {		//For each user in the champSel...
			Server.writeString(stream, champSel.getUser(i).getUserName());		//...sends the user name...
			stream.writeBoolean(champSel.getUserTeam(i));						//...and the team
		}
		
		u.sendToClient( packet );
	}

	public static void sendUpdatedChampionSelectData(ChampionSelect champSel) {
		//Send the update championSelect situation to all user in that championSelect
		
		Server.DebugLog("Send to every player in the champion select the last champion picked\n");
	
		ByteArrayOutputStream packet = new ByteArrayOutputStream();
		DataOutputStream stream = new DataOutputStream(packet);
		
		try {
			
			//Creates a new packet containing the Updated Champion Select Data
			msgToClient(stream,"UpdatedChampionSelectData");
			stream.writeShort( champSel.getChampion((short)(champSel.getUserSelecting()-1)) );
			
			for(short i=0; i<champSel.getPlayers(); i++)		//then send it to every user.
				champSel.getUser(i).sendToClient( packet );
			
		} catch(IOException e) {
			
		}
	}

	public static Game createGame(ChampionSelect c) {
		
		Game g = new Game(c);
		
		Game[] games = new Game[game.length+1];
		
		short i;
		for(i=0; i<game.length; i++)
			games[i] = game[i];
		
		games[i] = g;
		
		game = games;
		
		//Server send created game data
		
		Server.DebugLog("New Server Game created after Champion Select.\n");
		Server.DebugLog("Current Server Games: " + game.length + " \n");
		
		destroyChampionSelect(c);
		
		return g;
	}
	
	public static void destroyGame( Game g ) {
		
		boolean found = false;
		Game[] games = new Game[game.length-1];
		
		for(short i=0; i<games.length; i++) {
		
			if(game[i] == g)
				found = true;
			if(found)
				games[i] = game[i+1];
			else
				games[i] = game[i];
		}
		
		game = games;
		
		Server.DebugLog("ServerGame destroyed.\n");
		if(game != null) Server.DebugLog("Current Server Games: " + game.length + " \n");
		else Server.DebugLog("Current Server Games: 0\n");
	}
	
	public static Game findGame( User u ) {
		
		for(short i=0; i<game.length; i++) 
			if(game[i].isInGame(u)) return game[i];
		
		return null;
	}
	
	public static Game findGame( String s ) {
		
		for(short i=0; i<game.length; i++) 
			if(game[i].isInGame(s)) return game[i];
		
		return null;
	}

	public static void main(String[] args) {
		new Server();
	}
	
}