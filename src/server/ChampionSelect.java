package server;

public class ChampionSelect {

	private User[] players;
	private boolean[] teams;		//true -> Blue Team, false -> Red Team
	private short[] champions;
	
	private short userSelecting;
	
	public ChampionSelect(Lobby l) {
		
		players = new User[l.getCurrentPlayers()];
		teams = new boolean[players.length];
		champions = new short[players.length];
		userSelecting = 0;
		
		for(short i=0; i<champions.length; i++)
			champions[i] = -1;
		
		for(short i=0; i<players.length; i++) {
			players[i] = l.getUser(i);
			players[i].setStatus("ChampionSelect");
			teams[i] = l.getUserTeam(i);
		}
		
		User u;
		boolean b;
		
		for(short i=0; i<teams.length; i++ ) {	//re-order the players
			
			if( (i%2 == 0) && !teams[i] ) { 		//if there is a red in wrong position
				for(short j=(short)(i+1); j<teams.length; j++) { 		//search for a blue in the wrong position
					if( (j%2 == 1) && teams[j] ) {		//swap the two user
						u = players[i];
						players[i] = players[j];
						players[j] = u;
						b = teams[i];
						teams[i] = teams[j];
						teams[j] = b;
					}
				}
			}
			
			if( (i%2 == 1) && teams[i] ) {		//if there is a blue in wrong position
				for(short j=(short)(i+1); j<teams.length; j++) { 		//search for a red in the wrong position
					if( (j%2 == 0) && !teams[j] ) {		//swap the two user
						u = players[i];
						players[i] = players[j];
						players[j] = u;
						b = teams[i];
						teams[i] = teams[j];
						teams[j] = b;
					}
				}
			}
		}
		
		
	}
	
	public boolean isInChampionSelect(User u) {
		
		for(short i=0; i<players.length; i++)
			if(players[i].equals(u))
				return true;
		
		return false;
	}
	
	public int getPlayers() {
		return players.length;
	}
	
	public User getUser(short i) {
		return players[i];
	}
	
	public User[] getUsers() {
		return players;
	}
	
	public boolean getUserTeam(short i) {
		return teams[i];
	}
	
	public short getChampion(short i) {
		return champions[i];
	}
	
	public short getUserSelecting() {
		return userSelecting;
	}
	
	public void setChampion(short champ) {
		
		champions[userSelecting] = champ;
		userSelecting++;
		
		Server.sendUpdatedChampionSelectData(this);
		
		if(userSelecting == players.length) {	//All the players have picked a champion
			
			Server.createGame(this);
		}
	}
	
}
