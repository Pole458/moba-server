package server;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;

public class LogInThread implements Runnable {
	
	public User newUser;
	private ServerSocket ListenSocket;
	
	@Override
	public void run() {
		
		try {
			ListenSocket = new ServerSocket(Server.SERVERPORT, Server.MAXPLAYERS, InetAddress.getLocalHost());
		} catch (IOException e) {
			Server.DebugLog("IOException: " + e.getMessage() +" \n");
		}
		
		Server.DebugLog("Log in thread started\n");
		
		while(true) {
			
			try {
			
				Server.DebugLog("Waiting for new connection...\n");
				
				newUser = new User();

				newUser.Connect( ListenSocket.accept() );	//Wait for a new client to connect, then read his name
				
				Server.DebugLog("New connection found\n");
				
				ByteArrayOutputStream packet = new ByteArrayOutputStream();
				DataOutputStream stream = new DataOutputStream(packet);
				
				try {
					
					Server.msgToClient(stream, "Connected");	//Tells the User that he has successfully logged in
					
					Server.LogInUser( newUser );	//Try to Log In the new user
					
					newUser.StartConnection();		//If the new user is connected, start the connection threads
				
				} catch (LogInException e) {
					
					if( e.getMessage().equals("UserReconnected") ) { //Reconnect the user to his game
						
						e.game.sendReconnectData(stream, newUser);
						
					} else {
						
						packet = new ByteArrayOutputStream();
						stream = new DataOutputStream(packet);
						
						Server.msgToClient(stream, e.getMessage());	//Tell the client he can't log in
					
					}
					
					Server.DebugLog("LogInException: " + e.getMessage() +" \n");
				}
				
				newUser.sendToClient( packet );
				
			} catch (IOException e) {
				Server.DebugLog("IOException: " + e.getMessage() +" \n");
			}

		}
	}

}
