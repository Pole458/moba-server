package server;

import game.Game;

public class Lobby {
	
	public static final short MAXLOBBYPLAYERS = 6;
	
	private User[] players;
	private boolean[] team;		//true -> Blue Team, false -> Red Team
	private short currentPlayers;
	
	public Lobby() {
		
		currentPlayers = 0;
		players = new User[MAXLOBBYPLAYERS];
		team = new boolean[MAXLOBBYPLAYERS];
		
	}
	
	public Lobby(ChampionSelect c) {
		
		currentPlayers = 0;
		
		players = new User[MAXLOBBYPLAYERS];
		team = new boolean[MAXLOBBYPLAYERS];
		
		for(short i=0; i<c.getPlayers(); i++) {
			
			if( Server.findUser(c.getUser(i).getUserName()) != null) {		//it the user in still on line...
				players[currentPlayers] = c.getUser(i);						//...put him in the new lobby
				team[currentPlayers] = c.getUserTeam(i);
				
				players[currentPlayers].setStatus("Lobby");
				
				currentPlayers++;	
			}
		}
	
	}
	
	public Lobby(Game g) {
		
		currentPlayers = 0;
		
		players = new User[MAXLOBBYPLAYERS];
		team = new boolean[MAXLOBBYPLAYERS];
		
		for(short i=0; i<g.getPlayers(); i++) {
			
			if( Server.findUser(g.getPlayer(i).getUserName()) != null) {		//it the user in still on line...
				players[currentPlayers] = g.getPlayer(i);						//...put him in the new lobby
				team[currentPlayers] = g.getPlayerTeam(i);
				
				players[currentPlayers].setStatus("Lobby");
				
				currentPlayers++;
			}
		}
	}
	
	public void addUser( User u ) throws LobbyException {
		
		if( currentPlayers >= MAXLOBBYPLAYERS ) throw new LobbyException("The lobby is full");
		
		players[currentPlayers] = u;
		
		if(currentPlayers == 0) team[0] = true;				//Decides the team
		else team[currentPlayers] = !team[currentPlayers-1];
		
		currentPlayers++;
		
		Server.updateLobby(this);		//Send to all users in this lobby the updated lobby data
		
	}
	
	public void removeUser( User u ) {
		
		boolean found = false;
		
		if( currentPlayers == 1) Server.destroyLobby(this);
		else {
		
			for(short i=0; i<currentPlayers; i++) {
				
				if( found ) {
					players[i-1] = players[i];
					team[i-1] = team[i];
				}
				if(players[i].equals(u)) {
					found = true;
				}
			}
		}
		
		if(found) {
			currentPlayers--;
			Server.DebugLog("User exit the lobby: " + u.getUserName() + '\n');
			
			Server.updateLobby(this);
		}
		
	}
	
	public void switchTeam(User u) {

		Server.DebugLog(u.getUserName() + " is switching team\n");
		
		for(short i=0; i<currentPlayers; i++) {
			if( players[i].equals(u) ) {
				team[i] = !team[i];
				break;
			}
		}
		
		Server.DebugLog(u.getUserName() + " switched team\n");
	
		Server.updateLobby(this);
	}
	
	public boolean isInLobby(User u) {
		
		for(short i=0; i<currentPlayers; i++)
			if( players[i].equals(u) )
				return true;
		
		return false;
	
	}
	
	public User getUser(short i) {
		return players[i];
	}
	
	public boolean getUserTeam(short i) {
		return team[i];
	}
	
	public short getCurrentPlayers() {
		return currentPlayers;
	}
	
	public User lobbyOwner() {
		return players[0];
	}
	
}
