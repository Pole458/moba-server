package server;

@SuppressWarnings("serial")
public class LobbyException extends Exception {
	
	public LobbyException(String m) {
		super(m);
	}

}
