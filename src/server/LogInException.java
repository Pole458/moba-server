package server;

import game.Game;

@SuppressWarnings("serial")
public class LogInException extends Exception {
	
	Game game;
	
	public LogInException(String m) {
		super(m);
	}
	
	public LogInException(String m, Game g) {
		super(m);
		game = g;
	}
	
}
