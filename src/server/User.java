package server;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class User {
	
	private String userName;
	private String Status;
	
	public Socket ConnectionSocket;
	
	private Thread Input;
	
	private InputStream inStream;
	public DataInputStream inDataStream;
	
	private boolean sending;
	
	private OutputStream outStream;
	private DataOutputStream outDataStream;
	
	public User() {
		
		userName = "";
		Status = "MainPage";
		sending =  false;
		
	}
	
	public void Connect(Socket newSocket) throws IOException {
		
		inStream = newSocket.getInputStream();
		inDataStream = new DataInputStream(inStream);
		
		outStream = newSocket.getOutputStream();
		outDataStream = new DataOutputStream(outStream);
		
		userName = Server.readString( inDataStream );
		
		Input = new Thread(new InputThread(this), "User '" + userName + "' Input Thread");
		
	}
	
	public void StartConnection() {
		
		Status = "MainPage";
		
		Input.start();
	
	}
	
	public boolean equals(User u) {
		return userName.equals(u.getUserName());
	}
	
	public synchronized void sendToClient( ByteArrayOutputStream packet ) {
		
		synchronized(outDataStream) {
			
			if(sending)
				try {
					outDataStream.wait();
				} catch (InterruptedException e1) {
					Server.DebugLog(e1.getMessage()+"\n");
				}
			
			sending = true;
			
			try {	
				outDataStream.write( packet.toByteArray() );
			} catch (IOException e) {
				Server.DebugLog(e.getMessage()+"\n");
			}
			
			sending = false;
			outDataStream.notify();
		}
		
	}
	
	//GETTERS & SETTERS**********************************************************************************************
	public String getUserName() {
		return userName;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String s ) {
		Status = s;
	}
	//GETTERS & SETTERS**********************************************************************************************
	
	public class InputThread implements Runnable {
		
		private User user;
		
		public InputThread(User u) {
			user = u;
		}
		
		@Override
		public void run() {
			
			int inputByte;
			
			try {
				
				while(true) {
					try {
						
						inputByte = inDataStream.read();
						
						switch( inputByte ) {
					
						case 1:			//Create lobby
							Server.createLobby(user); 	//Creates a new lobby and adds the user to this lobby
							break;
					
						case 2:			//Exit Lobby
							Server.exitLobby(user); 	//Finds the user lobby and kick him out
							break;
							
						case 3:			//Invite to Lobby
							Server.sendInvite(user, Server.readString(inDataStream) );
							break;
							
						case 4:			//Invite Accepted
							Server.joinLobby(user, Server.findUser(Server.readString(inDataStream))); //Adds the invitedUser to the inviter lobby
							break;
							
						case 5:			//Start champion select
							Server.createChampionSelect(user);
							break;
							
						case 6:			//Selected champion
							Server.DebugLog("New Champion received.\n");
							Server.findChampionSelect(user).setChampion(inDataStream.readShort());
							break;
							
						case 7:			//Key Data
							Server.findGame(user).readKeyData(user);
							break;
							
						case 8:			//Switch Team
							Server.findLobby(user).switchTeam(user);
							break;
						
						}
					
					} catch (LobbyException e) {
						Server.DebugLog(e.getMessage() + '\n');
						Server.msgToClient(outDataStream,"LobbyException");
						Server.writeString(outDataStream, e.getMessage());
					}
				
				}
			} catch (IOException e) {
				Server.DebugLog(e.getMessage() + '\n');
				Server.LogOutUser(user);
			}
		}

	}
	
}


