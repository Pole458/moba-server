package menu;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class MyButton {
	protected Rectangle size;
	protected Color color;
	protected String name;
	protected boolean selected;

	public MyButton(String name) {
		this.name=name;
	}
	
	public MyButton(String name, Color color, Rectangle size) {
		this.size=size;
		this.color=color;
		this.name=name;
		this.selected=false;	
	}
	
	public void draw(Graphics g) {
		g.setColor(color);
		if(selected) g.fillRect((int)size.getX(),(int)size.getY(),(int)size.getWidth(),(int)size.getHeight());
		else g.drawRect((int)size.getX(),(int)size.getY(),(int)size.getWidth(),(int)size.getHeight());
		g.setColor(Color.BLACK);
		g.drawString(name,(int)size.getCenterX()-name.length()*4,(int)size.getCenterY()+6);
	}
	
	//SETTERS*******************************************************************************************************
	public void setPosition(short x, short y) {
		size.setLocation(x,y);
	}
	public void setSize(short w, short h) {
		size.setSize(w, h);
	}
	public void setSize(Rectangle size) {
		this.size = size;
	}
	public void setColor(Color color) {
		this.color = color;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setSelected(boolean s) {
		selected=s;
	}
	
	//GETTERS*******************************************************************************************************
	public Rectangle getSize() {
		return size;
	}
	public Color getColor() {
		return color;
	}
	public String getName() {
		return name;
	}
	public boolean isSelected() {
		return selected;
	}
}
