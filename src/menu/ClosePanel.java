package menu;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class ClosePanel extends JPanel implements MouseListener,MouseMotionListener {
	private JFrame jframe;
	private MyButton closeButton;
	private MyButton minimizeButton;
	private int posX, posY;
	
	public ClosePanel(JFrame jf) {
		posX=0;
		posY=0;
		jframe=jf;
		closeButton = new MyButton("X",new Color(0,154,215),new Rectangle(getWidth()-20,4,20,20));
		minimizeButton = new MyButton("_",new Color(0,154,215),new Rectangle(getWidth()-40,4,20,20));
		
		addMouseListener(this);
		addMouseMotionListener(this);
	}
	
	public void paint(Graphics g) {
		g.setColor(Color.DARK_GRAY);
		g.fillRect(0,0,getWidth(),getHeight());
		
		g.setColor(new Color(0,154,215));
		g.fillRect(0,0,getWidth(),4);

		g.drawString(jframe.getTitle(),5,18);
		
		closeButton.setSize(new Rectangle(getWidth()-20,4,20,20));
		closeButton.draw(g);

		minimizeButton.setSize(new Rectangle(getWidth()-40,4,20,20));
		minimizeButton.draw(g);
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		jframe.setLocation(e.getXOnScreen()-posX,e.getYOnScreen()-posY);
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		posX=e.getX();
		posY=e.getY();
		
		if(closeButton!=null)
			if(closeButton.getSize().contains(e.getX(),e.getY())) {
				System.exit(0);
			}
		
		if(minimizeButton!=null)
			if(minimizeButton.getSize().contains(e.getX(),e.getY())) {
				jframe.setState(JFrame.ICONIFIED);
			}
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		if(closeButton!=null)
			if(closeButton.getSize().contains(e.getX(),e.getY())) closeButton.setSelected(true);
			else closeButton.setSelected(false);
		
		if(minimizeButton!=null)
			if(minimizeButton.getSize().contains(e.getX(),e.getY())) minimizeButton.setSelected(true);
			else minimizeButton.setSelected(false);
	}
	
	@Override
	public void mouseClicked(MouseEvent arg0) {}

	@Override
	public void mouseEntered(MouseEvent arg0) {}

	@Override
	public void mouseExited(MouseEvent arg0) {
		closeButton.setSelected(false);
		 minimizeButton.setSelected(false);
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {}
	
}
