package menu;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

@SuppressWarnings("serial")
public class ServerPanel extends JPanel implements MouseListener, MouseMotionListener {
	private JTextField portField;
	private MyButton createButton;
	private MyButton minButton;
	private MyButton plusButton;
	private short players;
	
	public ServerPanel() {
		setLayout(null);
		addMouseListener(this);
		addMouseMotionListener(this);
		
		players=2;
		
		portField = new JTextField("1900");
		createButton = new MyButton("CREATE",new Color(0,154,215),new Rectangle(100,125,100,25));
		minButton = new MyButton("-",new Color(0,154,215),new Rectangle(150,75,25,25));
		plusButton = new MyButton("+",new Color(0,154,215),new Rectangle(200,75,25,25));
		add(portField);
		
		portField.setHorizontalAlignment(SwingConstants.CENTER);
		portField.setBackground(Color.DARK_GRAY);
		portField.setForeground(Color.BLACK);
		Border border = BorderFactory.createLineBorder(new Color(0,154,215));
		portField.setBorder(border);
		
		portField.setBounds(new Rectangle(175,25,50,25));
	}
	
	public void paint(Graphics g) {
		g.setColor(Color.DARK_GRAY);
		g.fillRect(0,0,getWidth(),getHeight());
		
		g.setColor(Color.BLACK);
		g.drawString("Port:",75,50);
		g.drawString("Players:",75,100);
		g.drawString(String.valueOf(players),185,93);
		
		g.setColor(new Color(0,154,215));
		g.drawLine(70,55,230,55);
		g.drawLine(70,105,230,105);
		g.drawRect(175,75,25,25);
		
		if(players>2) minButton.draw(g);
		else minButton.setSelected(false);
		if(players<8) plusButton.draw(g);
		else plusButton.setSelected(false);
		
		if(createButton!=null) createButton.draw(g);
		
		paintComponents(g);
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		if(createButton.getSize().contains(e.getX(),e.getY()) ) {
			synchronized(this) {
				notify();
			}
		}
		
		if(minButton.getSize().contains(e.getX(),e.getY()) ) {
			if(players>2) {
				players-=2;
			}
		}
		
		if(plusButton.getSize().contains(e.getX(),e.getY()) ) {
			if(players<8) {
				players+=2;
			}
		}
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		if(createButton.getSize().contains(e.getX(),e.getY()) ) createButton.setSelected(true);
		else createButton.setSelected(false);
		
		if(players>2)
			if(minButton.getSize().contains(e.getX(),e.getY()) ) minButton.setSelected(true);
			else minButton.setSelected(false);
		
		if(players<8)
			if(plusButton.getSize().contains(e.getX(),e.getY()) ) plusButton.setSelected(true);
			else plusButton.setSelected(false);
	}
	
	@Override
	public void mouseClicked(MouseEvent arg0) {}

	@Override
	public void mouseEntered(MouseEvent arg0) {}

	@Override
	public void mouseExited(MouseEvent arg0) {}

	@Override
	public void mouseReleased(MouseEvent arg0) {}

	@Override
	public void mouseDragged(MouseEvent arg0) {}
	
	public int getPort() {
		return Integer.valueOf(portField.getText());
	}
	
	public short getPlayers() {
		return players;
	}

}
